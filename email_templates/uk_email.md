Subject: Employment & Payroll Forms for GitLab Ltd (UK)

Hi PERSON,

We are so excited to have you join GitLab! So that we can add you to our payroll system as quickly as possible, please complete the following checklist of tasks:

* If you do not have a National Insurance (NI) number please apply for one immediately following the instructions on [this website](https://www.gov.uk/apply-national-insurance-number). You will enter it in our system on your first day.
* If you have a P45 from your previous company please email it to me.
* If you do not have a P45, then you must complete the [New Starter Checklist](https://public-online.hmrc.gov.uk/lc/content/xfaforms/profiles/forms.html?contentRoot=repository:///Applications/PersonalTax_A/1.0/SC2&template=SC2.xdp). When you have completed it, please save it as a pdf and either: 
   * Print, sign, scan and email it to me or 
   * Upload the pdf to a free HelloSign account that you create yourself, sign electronically, save as a pdf and email it to me.
* Please complete the GitLab Ltd UK payroll form that will be sent to you from the HelloSign system.

Thank you PERSON!
