#### For Security Only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.
1. [ ] New team member: Request a light agent ZenDesk account: [support handbook](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff)

</details>


<details>
<summary>Manager</summary>

1. [ ] Manager: Add new team member to `security-gl-team@gitlab.com`.
1. [ ] Manager: Invite new team member to the private security team Slack channels.
1. [ ] Manager: Add new team member to the [gl-security](https://gitlab.com/gitlab-com/gl-security) GitLab group.
1. [ ] Manager: Add the new team member to the relevant calendars (ex: "Security Engineering and Research").

</details>
