#### People Experience 

1. [ ] Search for `<!-- Intern` in this issue and follow the notes. 
1. [ ] Ping @csotomango or @Javonna to create the timesheet

#### New Team Member
1. [ ] Create a Reminder on your calendar to Complete the [Intern Feedback Form](https://docs.google.com/forms/d/e/1FAIpQLSffF8sAFDHTp0JAeum9XYHaG_PsFSHREssSpGECMeJJClXVrg/viewform). 

