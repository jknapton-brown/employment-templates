#### Customer Success

<details>
<summary>Manager</summary>

1. [ ] Manager: Notify the new member that they should be invited to a Sales Quick Start Classroom which is their Customer Success onboarding experience. 
1. [ ] Manager: Invite to sales meeting.
1. [ ] Manager: Schedule weekly 1:1 meeting.
1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing) in our Google Docs.

</details>


<details>
<summary>New Team Member</summary>

1. [ ] New team member: In the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing), familiarize yourself with: [Our Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit) and [Competition](https://about.gitlab.com/comparison/).
1. [ ] New team member: Watch all the customer success courses listed in the [sales training handbook](https://about.gitlab.com/handbook/sales-training/#customer-success-cst-courses).
1. [ ] New team member: Review the [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/).

</details>


