### Day 1 - For New Team Members in the UK 

<details>
<summary>People Experience</summary>

Before Start Date
1. [ ] People Experience: A few days before new team member's start date, send the new team member the [UK email](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/blob/master/.gitlab/issue_templates/uk_email.md) with New Starter Checklist link, information about P45 and notification that UK payroll form will be sent from Docusign and include that if this is not completed within the first week of employment, unfortunatly it can result in a delay in payroll.
1. [ ] People Experience: At the same time you send the the email in step 1, using the Docusign, send the new team member the UK Payroll Form. To do this go to Template >> Use template >> `GitLab Ltd UK Vistra Payroll Form`. Fill in as much information as possible from BambooHR and stage in Docusign for signature using their personal email.
1. [ ] People Experience: Once the payroll form has been completed and signed by the new team member file the document in BambooHR under Documents/Payroll Forms. 
1. [ ] People Experience: If a New Starter Checklist was completed, save it in the same folder. If new team member has a P45, please save it there as well.
1. [ ] People Experience: When the Payroll form and P45 or New Starter Checklist are complete, send as encrypted attachments to Vistra Payroll along with an encrypted copy of the team member's signed contract. The signed contract is found in the Contracts & Changes folder in the team member's BambooHR profile. This information should not be sent later than Day 1 of a new hire's start date. Do not cc the team as this is sensitive information. Once acknowledged, delete your email.

After Start Date
1. [ ] People Experience: Double check that the `Employment Status` in BambooHR has three entries:   
   * Effective Date: `Hire Date`  Employment Status: `Probationary Period`  Comment: `6-month Probationary Period until YYYY-MM-DD (6 months after hire date)` 
   * Effective Date: `6 months after Hire Date` Employment Status: `End of Probation Period` Comment: End of Probationary Period
   * Effective Date: `6 months + 1 day after Hire Date` Employment Status: `Active` Comment: No need to comment.
1. [ ] People Experience: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the People Ops Analyst. State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.
1. [ ] People Experience: If the team member requests to be added to the AXA medical insurance scheme, please follow the [directions in the handbook](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/ltd-benefits-uk/#medical-insurance) and forward the request to our UK HR partners by email. Please inform the team member that receiving a reply back from AXA can take 7 - 10 working days from original email sent. 

</details>



<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Confirm that you have sent your P45 or New Starter Checklist to your assigned People Experience Associate. (Please be aware that not completing one of these two documents and sending them to your PEA at the latest during your first week of employment, can result in a delay in payroll.)
1. [ ] New Team Member: Read through the [AXA PPP Brochure](https://drive.google.com/a/gitlab.com/file/d/0Bwy71gCp1WgtUXcxeFBaM0MyT00/view?usp=sharing) and let People Experience team know if you (and your dependents) would like to join the medical insurance scheme by emailing them at `total-rewards@gitlab.com`. You can find some more information on the [benefits section](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/ltd-benefits-uk/#medical-insurance) also. This does not currently include dental or optical care.      
   - Please also note that this is a P11d taxable benefit.
   - Please note that once the Total Rewards team have submitted the request, it can take 7 -10 working days before receiving a reply from AXA. 
1. [ ] New Team Member: Please confirm that you have seen and read the [Certificate of Employers Liabilty Insurance](https://drive.google.com/drive/folders/0By9jsAUY_oNqdFlFQVc0U1EtSkU).
1. [ ] New Team Member: Please read through the auto-enrollment personal pension details which you can find on the [pensions section](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/ltd-benefits-uk/#pension-introduction) of the benefits page.
1. [ ] New Team Member: Please confirm that you have read the [Homeworking Policy](https://docs.google.com/document/d/1zUNgTR-PBPNbL0mNJMNs8kSDOJm_VDqil_Rv3EHAb2E/edit?usp=sharing)

</details>

