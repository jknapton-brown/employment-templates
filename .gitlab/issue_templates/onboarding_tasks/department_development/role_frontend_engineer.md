#### Frontend Engineers

<details>
<summary>Manager</summary>

1. [ ] Create a Frontend Engineer Training Issue in the [Frontend Onboarding Issue Tracker](https://gitlab.com/gitlab-org/frontend/onboarding/issues). Use the `Frontend_onboarding.md` template. Assign the issue to yourself, the manager and their FE onboarding buddy.


</details>
