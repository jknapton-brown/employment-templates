### For New Team Members in the Netherlands 

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Confirm in this issue with a comment that you completed the payroll form for HRSavvy during your offer signing process. This info is incredibly important to ensure you are uploaded to payroll.
1. [ ] New Team Member: Confirm with a comment in this issue that you completed the [wage tax form](https://drive.google.com/a/gitlab.com/file/d/1q8N-idoYGFSCw2ajat9RscfQ004EL-PS/view?usp=sharing).
1. [ ] New Team Member: If you don't have a BSN number you will need to apply for one asap. Details on that process can be found under the BSN number section on the [visas page in the handbook](https://about.gitlab.com/handbook/people-operations/visas/#bsn-number). If you are already informed of this by HRSavvy, please comment in this issue confirming this. 
1. [ ] New Team Member: To get your internet subscription [reimbursement](https://about.gitlab.com/handbook/people-group/people-experience-team/#regeling-internet-thuis-form), fill in and sign the [Regeling Internet Thuis form](https://docs.google.com/a/gitlab.com/document/d/1J70geARMCjRt_SfxIY6spdfpTbv_1v_KDeJtGRQ6JmM/edit#heading=h.5x5ssjstqpkq) and email it to the People Experience team at `people-exp@gitlab.com`. Also attach a receipt of your latest bill. The People Experience team will then send them to HR Savvy Payroll in the Netherlands via email. 

</details>

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Double check that the `Employment Status` in BambooHR has five entries:   
   * Effective Date: `Hire Date`  Employment Status: `Temporary Contract`  Comment: `1-year Temporary Agreement until YYYY-MM-DD (12 months after Hire Date)` 
   * Effective Date: `12 months after Hire Date` Employment Status: `Temporary Contract Ends` Comment: End of Temporary Agreement
   * Effective Date: `Hire Date`  Employment Status: `Probationary Period`  Comment: `1-month Probationary Period until YYYY-MM-DD (1 month after Hire Date)` 
   * Effective Date: `1 month later` Employment Status: `End of Probation Period` Comment: End of Probationary Period
   * Effective Date: `12 months + 1 day after Hire Date` Employment Status: `Active` Comment: No need to comment.

**Important:** For a 12-month contract with a March 4, 2019 start date, the end date would be March 3, 2020. Any following contract after this 12-month period would have a start date of March 4, 2020.

1. [ ] People Experience: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Experience: Check above to see if team member has completed the Regeling Internet Thus form task. If so, and they email their form and latest bill to `people-exp@gitlab.com`, please forward to HR Savvy Payroll, stating team member name and start date.
1. [ ] People Experience: If this was not done during the offer signing process and the new team member confirms this in a comment in this issue, send the payroll form and wage tax form to HRSavvy along with the contract of employment, copy of the residence permit (if applicable), copy of the debit card, and passport. All forms should be filed in BambooHR, reach out to HRSavvy to get the forms and file them.
1. [ ] People Experience: If the position is in development or research and involves `writing code to create new software.`, it likely qualifies for [WBSO (R&D tax credit)](https://about.gitlab.com/handbook/people-operations/#wbso-rd-tax-credit-in-the-netherlands); ping the Director of Tax in this issue so that they may review if team member should be added to the [WBSO sheet](https://docs.google.com/spreadsheets/d/1pNprxjsyJA45wGV38zGVyhotKWRJ99iQ8CvRZtz_H8w/edit?ts=5d2c3441#gid=0). Director of Tax will add team member to WBSO sheet if applicable. 
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the People Ops Analyst. State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.

</details>
