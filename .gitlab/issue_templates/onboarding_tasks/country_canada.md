### Day 1 - For team members in Canada only

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Please read the [Canada Corp Benefits](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/canada-corp-benefits/) handbook page.
1. [ ] New Team Mmber: FYI _no action required_, the Payroll team will send your payment information to our payroll provider as soon as you are added to our HRIS (BambooHR).

</details>

<details>
<summary>Total Rewards</summary>

1. [ ] Total Rewards Analyst (@SVBarry): Add team member to benefits platform, [Collage](https://www.collage.co/).

</details>

<details>
<summary>Payroll</summary>

1. [ ] Payroll (@sszepietowska): Add new team member to Canada payroll and send invitation to team member.

</details>

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Experience: When choosing the Time Off Employee Accrual Policy, please be aware that GitLab Canada Corp has three policies. More information can be found on the [PTO handbook page](https://about.gitlab.com/handbook/paid-time-off/#statutory-vacation-requirements).
    - Canada Corp (All except SK, QC): For all team members who live in Provinces or Territories other than Saskatchewan and Quebec. `Accrual Level Start Date` and `Accrual Start Date` are the team member's start date other than the 3 provinces listed next. Any team member that lives in British Columbia or Newfoundland or Labrador needs to have their `Accrual Level Start Date` and `Accrual Start Date` be their 5th work day. Example: If a team member in BC has a start date of 2020-03-09, their `Accrual Level Start Date` and `Accrual Start Date` must be 2020-03-13.
    - Canada Corp (Saskatchewan Only): For all team members who live in the Province of Saskatchewan. `Accrual Level Start Date` and `Accrual Start Date` are the team member's start date.
    - Canada Corp (Quebec Only): For all team members who live in the Province of Quebec. `Accrual Level Start Date` and `Accrual Start Date` are the team member's start date.
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the People Ops Analyst. State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.

</details>
