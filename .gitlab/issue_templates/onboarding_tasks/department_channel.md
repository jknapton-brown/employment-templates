#### Channel Department

<details>
<summary>Sales Enablement</summary>

1. [ ] Sales Enablement (@tparuchuri): Add new Sales Team Member to Sales Quick Start Learning Path in Google Classroom.
New sales team member will receive an email prompting them to login to Google Classroom to begin working through the Sales Quick Start Learning Path.
This learning path is designed to accelerate the new sales team member's time to productivity by focusing on the key sales-specific elements they need to know and be able to do within their first several weeks at GitLab.

</details>

<details>
<summary>Manager</summary>

1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing) in our Google Docs.
1. [ ] Manager: Inform Sales Operations (@tav_scott @james_harrison) what territory the new team member will be working and if they have a paired SDR.
1. [ ] Manager: Create and submit an issue using [this issue template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/issues/new#) to inform Sales Ops of the territory that the new team member will assume, paired SDR (if any), and identified opportunity holdovers that should remain with the current owner (if any).
Accounts will be reassigned to new hire and transition plan needs to be approved per account.
1. [ ] Manager: Slack Sales Analytics (@Swetha) the new team member's quota information for upload into Salesforce.

</details>

<details>
<summary>Sales Strategy</summary>

1. [ ] Sales Strategy (@Swetha): Set BCR, SCR (if applicable), and prorated quota and deliver participant plan/schedule
1. [ ] Sales Strategy (@Swetha): Deliver Participant Schedule

</details>

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Complete your Sales Quick Start Learning Path (see your email for an invitation from Google Classroom).
If you have questions, please reach out to Tanuja (@tparuchuri).
1. [ ] New team member: Follow [these instructions](https://help.datafox.com/hc/en-us/articles/227081328-User-Setup-Connect-your-DataFox-User-to-your-Salesforce-Account) to link your DataFox Account with your Salesforce Account.
1. [ ] New team member: For all roles EXCEPT PubSec Inside Sales Rep and SMB Customer Advocate, please consult with your manager to determine whether or not you need access to Zendesk Light Agent.
If yes, follow [these instructions](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff) to request access.
Note: SMB Customer Advocates are provisioned "GitLab Staff Role" access to Zendesk and PubSec Inside Sales Reps are provisioned "Light Agent" access to Zendesk-federal within their role-based access request template above.
PubSec Strategic Account Leaders may rely on their PubSec Inside Sales Rep for Zendesk-federal related matters.

</details>

<details>
<summary>Sales Ops</summary>

1. [ ] Marketing (@tav_scott): Two weeks after start date, go live with update to Territory Model in LeanData with new territory assignments.

</details>

* Job Specific Tasks
  * Add yourself to the following Slack Channels:
- [ ] channel-fyi
- [ ] channel-programs-ops
- [ ] channel-services
- [ ] channel-sales
- [ ] channels-emea
- [ ] channels-amer
- [ ] channel-japan-sales
* Complete the following Channel Specific Onboarding Enablement:
- [ ] [Working With and Co-Selling With Partners](https://gitlab.edcast.com/insights/ECL-9a30df21-dff0-4ac5-8ffa-e67429860dfd)
- [ ] [Alliances: Working With and Co-Selling With AWS](https://gitlab.edcast.com/insights/ECL-848bf136-9c27-4c03-99cf-0fdc35184e40)
- [ ] [Alliances: Working With and Co-Selling With IBM](https://gitlab.edcast.com/insights/ECL-47a1907b-dace-4d81-b8a6-3761b4b98820)
- [ ] [Alliances: Working With and Co-Selling With Google](https://gitlab.edcast.com/insights/ECL-3b7cd71f-a309-461b-ad40-c147f76d5a3e)
- [ ] [Building Pipeline with your Partners - MDF and Marketing Campaigns](https://gitlab.edcast.com/insights/ECL-97e5297d-4067-45c4-a9d4-fac241775d85)
- [ ] [Planning for Joint GitLab and Partner Success](https://gitlab.edcast.com/insights/ECL-fad02cd0-772a-4d6e-9cd8-37b9e95af7e6)
- [ ] [Growing Our Partner Business](https://gitlab.edcast.com/insights/ECL-9f2e7565-0f2d-4328-ba7a-ebb76c137615)
- [ ] [GitLab Deal Registration and Rules of Engagement Training](https://youtu.be/R5BSo92T8sw)
- [ ] [REVIEW: FY22 GitLab Channel Program Updates](https://www.youtube.com/watch?v=6Ngt3Pit9S4)
- [ ] [REVIEW: FY22 GitLab Channel Tagging and Reporting](https://www.youtube.com/watch?v=JSzWVQjIChM)
- [ ]  Request Admin Access to Partner Portal (ImPartner) via emailing partnerhelpdesk@gitlab.com 
- [ ]  Review [Channel Partner Program Handbook Page](https://about.gitlab.com/handbook/resellers/)
- [ ]  Review [Channel Sales Handbook Page ](https://about.gitlab.com/handbook/sales/channel/)
- [ ]  Review [Channel Operations Handbook Page](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/)
- [ ]  Review [Channel Services Handbook Page](https://about.gitlab.com/handbook/resellers/services/) 
- [ ]  Review [Channel Training, Certification and Enablement Handbook Page](https://about.gitlab.com/handbook/resellers/training/) 
- [ ]  Review [GitLab Alliances Handbook Page](https://about.gitlab.com/handbook/alliances/) 
- [ ]  Review [GitLab Alliances INTERNAL ONLY Handbook Page](https://gitlab-com.gitlab.io/alliances/alliances-internal/) 
* Weeks 2 and 3 of general onboarding, schedule Coffee Chats with:
- [ ] VP WW Channels
- [ ] Senior Director Partner Enablement and Channel Programs
- [ ] Director Channel Operations
- [ ] Regional Sales Director
- [ ] Head of Channel Sales (APAC)
