### Welcome to your Career Mobility Issue!

## Introduction

The Career Mobility Issue, which was compiled to offer you support and guidance as you cross-board into your new role within GitLab, is split out into four areas of responsibility - namely those of the **Team Member** (`__TEAM_MEMBER_HANDLE__`); those of the **Previous Reporting Manager** (`__PREVIOUS_MANAGER_HANDLE__`); those of the **New Reporting Manager** (`__MANAGER_HANDLE__`) and finally those of the **People Experience Associate Team** (`__PEOPLE_EXPERIENCE_HANDLE__`).

If any of the tasks assigned to you seem confusing and you aren't sure how to proceed please be sure to let the People Experience Team know via the #peopleops Slack channel. Alternatively, if you are unable to work on a task because you are waiting on another stakeholder to close out one of theirs, feel free to ping them right here in this issue.  

In some instances your **New Reporting Manager** may opt to assign you a **Career Mobility Buddy** as an extra point of support, if this is the case please don't hesitate to route any queries you may have in their direction too.

### What to Expect

The People Experience Team has identified a set of tasks that will make your cross-boarding easy and efficient.  You are encouraged to complete each task over the course of two weeks paying careful attention to those which are compliance-related (more information on that below).

| Area of Focus |
| --- |
| Profile Updates (Internal and External) |
| GitLab Values Check-In (Career Mobility) |
| Career Mobility Handover |
| Competency Refreshers |
| Role-Based Tasks |

### Cross-Boarding Compliance
It is important to note that certain tasks within your Career Mobility Issue relate to compliance and ultimately keeping GitLab secure.  These tasks are marked with a **red circle** and completion of them is subject to audit by both the People Experience Team and various other stakeholders such as **Security** and **Payroll** so please be sure to acknowledge them once complete by checking the relevant box.

### Continuous Improvement
In the interests of paying it forward and contributing to our core values of Collaboration and Efficiency - we encourage you to suggest changes and updates to this issue and the handbook all of which can be done via Merge Request (MR).

-----

### Pre Cross-Boarding Tasks

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Once the People Experience Associates have been notified by the People Operations Analyst in the Promotion/Transfer Tracker spreadsheet that migration of a team member into a new role is forthcoming, create a **confidential** issue called 'Career Mobility (NAME), per (DATE, please follow yyyy-mm-dd) as (TITLE)' in the [People Ops Employment Issue Tracker](https://gitlab.com/gitlab-com/people-ops/employment/issues) with relevant sections of this checklist, be it Promotion, Demotion or Migration to a New Role.
1. [ ] People Experience: Check if the due date is correctly added (two weeks from the effective date of migration i.e. the date on which the team member will start their new role) by the Employment bot.
1. [ ] People Experience: Previous Manager is `__PREVIOUS_MANAGER_HANDLE__`, new Manager is `__MANAGER_HANDLE__`, and People Experience is tackled by `__PEOPLE_EXPERIENCE_HANDLE__`. Check if the issue is assigned to the People Experience team member, the migrating team member, both the Previous and New Reporting Managers and the relevant People Business Partner.
1. [ ] Add the following comment to the issue tagging the team member, previous manager and new manager: 
      - `We have opened this Career Mobility issue as a guide to the transition to your new role. There are tasks for you, your previous manager (`__PREVIOUS_MANAGER_HANDLE__`) and your new reporting manager (`__MANAGER_HANDLE__`). The issue due date is set to 2 weeks, and there are certain aspects that need to be completed from a compliance perspective (for example Access Requests). Please reach out to me if you have any questions! :smile:  
1. [ ] People Experience: Check that the New Manager has linked the relevant access requests in this issue for compliance.

</details>

<details>
<summary>Previous Reporting Manager</summary>

1. [ ] Previous Reporting Manager: Review the team member's current access and submit an [Access Change Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=Access_Change_Request) issue. Please work with the New Reporting Manager to determine if a team member's access will still be needed in the new role or if access will need to be updated to a higher/lower access role. Please cc the new manager.
      1. [ ] Please inform MktgOps/Outreach provisioners in the access request if the team member is an Outreach user. Any changes to roles in SFDC must be remapped in Outreach.
1. [ ] Coordinate with IT Ops to change any shared passwords, with the intent to be gated behind Okta when possible, in particular;
   1. [ ] Review if the team member has sysadmin access passwords for GitLab.com Infrastructure (ssh, chef user/key, others). Identify if any can be moved to Okta.
   1. [ ] Review what 1Password vaults team member had access to, and identify any shared passwords to be changed and moved to Okta. If the team member has access to any shared passwords that will no longer be relevant to their new role, please ping  `@gitlab-com/business-technology/team-member-enablement` and coordinate shared account password rotation.
1. [ ] Previous Reporting Manager: If they will become or will no longer be a provisioner of a tool, open an issue to [Update the Tech Stack Provisioner.](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=Update_Tech_Stack_Provisioner) Please cc the new manager.
1. [ ] Previous Reporting Manager: Arrange a 2:1 handover session with the New Reporting Manager and transitioning Team Member. It is recommended (but not required) to complete the [Career Mobility Handover Template](https://docs.google.com/document/d/1iRbmS518CDjmhZEjvkaibqR1g0angWE83sWvtlH-elE/edit#heading=h.a3i4gy6pj4y7) outlining the team members strengths and accomplishments, goals, and improvement areas. Documenting this information is an important part of the process to commemorate the team member's role transition. 360 feedback may also be reviewed/discussed in this session. _Note: The handover can also take place [asynchronously](/handbook/communication/#asynchronous-communication) if preferred._
1. [ ] Previous Reporting Manager: Review whether [a probationary period is included in the location of the team member](https://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#probation-periods-of-team-members-employed-through-a-peo) and whether it has ended or is still running. If still running ensure to handover any considerations for the probationary period to the New reporting manager. 

</details>

<details>
<summary>New Reporting Manager</summary>

1. [ ] Manager: Review the team member's current access and submit an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=Single_Person_Access_Request) or choose from a role-based template in the [new issue dropdown](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues).
1. [ ] Manager: Be sure to link any Access Requests created to this Career Mobility Issue for ease of reference and in support of internal security compliance mechanisms.
1. [ ] Manager: Select a Career Mobility Buddy on the team member's new team. Please try to select someone in a similar time zone, and someone who has been at GitLab for at least 3 months.
1. [ ] Manager: Send an email to the chosen buddy `MANAGER__PLACE__BUDDY_HANDLE__HERE` letting them know that they have been chosen, and include a link to the onboarding issue.
1. [ ] Manager: Schedule a video call using Zoom with the team member at the start of their first day to welcome them to the team and set expectations.
1. [ ] Manager: Organize smooth onboarding with clear starting tasks/pathway for the new team member.
1. [ ] Calendars & Agenda
   1. [ ] Manager: Invite team member to recurring team meetings.
   1. [ ] Manager: Schedule weekly [1:1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with new team member. Use the first 15 minutes to get to know your new team member on a personal level.
1. [ ] Manager: Check this box in acknowledgment that the Previous Reporting Manager has scheduled a 2:1 handover session with the new team member. 
1. [ ] Manager: Set new GitLab team members' project-level permissions as-needed.
1. [ ] Manager: Add the new team member to the required groups in 1Password if you have access to the Admin Console in 1Password. If you do not have access please ping @gitlab-com/business-technology/team-member-enablement with which vaults the new team member should be added to. Note: if it is necessary to add an individual to a vault (instead of to a group), make sure that the permissions are set to _not allow_ exporting items.
1. [ ] Manager: Be sure to introduce the transitioning team member in the next team call. Ask them to share insights into their professional background, their interest in joining the team, where they are located, and what activities they enjoy outside of work.
1. [ ] Manager: Check whether [a probationary period is included in the location of the team member](https://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#probation-periods-of-team-members-employed-through-a-peo) and whether it has ended or is still running. If still running ensure to gather information needed to make the decision when that probationary period ends.  


</details>

<details>
<summary>Career Mobility Buddy</summary>

1. [ ] Buddy: Review the [Onboarding Buddies handbook page](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/) to get familiar with your role. These guidelines were written for onboarding but many can still apply to assist in career mobility. 
1. [ ] Buddy: Schedule a zoom call for their first day to introduce yourself. Make sure they know that if they have questions, they can come to you. Guide them to the right parts of the handbook, and/or encourage them to ask their questions of the wider group in Slack, etc.
1. [ ] Buddy: Schedule at least one follow-up chat after one week. 

</details>

### Team Member Tasks

<details>
<summary>Team Member</summary>

#### Continuous Improvement

1. [ ] Team Member: Create a retro :recycle: thread to capture any feedback and/or comments that you will collect while working on this issue

#### Career Mobility Values Check-In

1. [ ] Team Member: Complete the [Values Check-In (Career Mobility)](https://docs.google.com/forms/d/e/1FAIpQLSciWfxj_Wj0IgbVzylpPKM9WEFyc0z4sD0cN6GfAn9tNyQi_A/viewform?usp=sf_link) which centers predominantly on the role you will be transitioning from.

#### Competency Refresher

Review the [Competencies Handbook Page](https://about.gitlab.com/handbook/competencies/) which highlights three key categories of competency within GitLab namely [Values Competencies](https://about.gitlab.com/handbook/competencies/#values-competencies); [Remote Work Competencies](https://about.gitlab.com/handbook/competencies/#remote-work-competencies) and [Functional Competencies](https://about.gitlab.com/handbook/competencies/#functional-competencies).

1. [ ] If you have not done so already, complete the [GitLab All-Remote Certification](https://about.gitlab.com/company/culture/all-remote/remote-certification/) (Remote Work Foundation) which was designed to give New Managers and Individual Contributors an opportunity to master all-remote business concepts and build key skills in remote subject areas.
1. [ ] If you have not done so already, complete the [Diversity; Inclusion and Belonging Certification](https://gitlab.edcast.com/journey/dib-training-certification). 
1. [ ] If you have not done so already, complete the [GitLab 101 Tool Certification](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/gitlab-101/#introduction).  This is the pre-requisite for the [GitLab 201 Tool Certification](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/gitlab-201/) which is a great opportunity to extend your knowledge.

#### Profile Updates 

1. [ ] Team Member: Update team page entry. Instructions here in [the handbook](https://about.gitlab.com/handbook/git-page-update/#11-add-yourself-to-the-team-page). Assign the Merge Request to your manager for merging. If your manager doesn't having merging rights, assign to the People Experience Associate assigned to this issue.
1. [ ] Team Member: Update your [Slack profile](https://gitlab.slack.com/account/profile) to include your new role.
1. [ ] Team Member: Update your GitLab profile to include your new role.
1. [ ] Team Member: Update your Gmail signature to include your new role. [Here is an example you can use](https://about.gitlab.com/handbook/tools-and-tips/#sts=Email%20signature).
1. [ ] Team Member: Update your [Zoom profile](https://zoom.us/profile) to include your new role.
1. [ ] Team Member: Once you have completed the first two weeks in your new role along with all of the tasks in your Career Mobility Issue, please take a moment to provide the People Experience team with feedback on how your transition went and where the process can potentially by completing the [Career Mobility Survey.](https://docs.google.com/forms/d/e/1FAIpQLSdhH9vJ_Ztf0fR6MI3U165EJn6mytBk2gbC2wG0B381IpBfyw/viewform) - we also encourage you to make suggested changes or updates via a merge request.
1. [ ] Team Member: Once all of the tasks have been completed, close this issue.
</details>

---

### Role-specific tasks



---

<!-- include: division_tasks -->

---

<!-- include: department_tasks -->

---

<!-- include: role_tasks -->


/label ~"career-mobility"
