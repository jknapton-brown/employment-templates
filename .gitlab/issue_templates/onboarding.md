## Welcome to your GitLab Onboarding Issue!

We are so excited you’re here and look forward to setting you up for [all-remote](https://about.gitlab.com/company/culture/all-remote/guide/) success as your team member journey begins.\
**Note:** The wealth of information provided in this issue can be overwhelming at times. It's okay to skim through resources, bookmark them and come back at a later time.

### Table of Contents
* [Accounts and Access](#account-and-access)
* Day 1: [Getting Started: Accounts and Paperwork](#day-1-getting-started-accounts-and-paperwork)
* Day 2: [Morning: Remote Working and our Values](#day-2-remote-working-and-our-values)
* Day 3: [Morning: Security & Communication](#day-3-security-communication)
* Day 4: [Morning: Social](#day-4-social)
* Day 5: [Morning: Git](#day-5-git)
* [Weeks 2 - 4: Explore](#weeks-2-4-explore)
* [Job Specific Tasks](#job-specific-tasks)

### Introduction

This is your core GitLab onboarding issue and it is housed in GitLab.com in the interests of what is known as [dogfooding](https://about.gitlab.com/handbook/values/#dogfooding).  It has been crafted with the [three dimensions of remote onboarding](https://about.gitlab.com/company/culture/all-remote/onboarding/) in mind, namely: Organizational Onboarding, Technical Onboarding and Social Onboarding.

All three of these are underpinned by the sentiments of [self directed and continuous learning](https://about.gitlab.com/company/culture/all-remote/self-service/), using the diverse set of high and low touch resources available to all GitLab team members such as videos; handbook pages; blog pieces; Zoom sessions and knowledge assessments, while encouraging [asynchronous communication and workflows](https://about.gitlab.com/company/culture/all-remote/asynchronous/).

### Support and our Single Source of Truth

Since GitLab is a [handbook first](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/) organization, the answer to any question you may have particularly throughout your first thirty days should be documented and readily available as this is our [Single Source of Truth (SSoT)](https://about.gitlab.com/handbook/values/#single-source-of-truth).

If you are unable to find the information you are looking for, please be sure to reach out to your Manager (`__MANAGER_HANDLE__`); Onboarding Buddy (`__BUDDY_HANDLE__`) or People Experience Associate (PEA) (`__PEOPLE_EXPERIENCE_HANDLE__`) for support by tagging them in the comments section of this issue. If you need to troubleshoot specific issues with tools, you can add a screenshot of the issue directly in the comment to help them diagnose the problem.

Alternatively once you are active on [Slack](https://about.gitlab.com/handbook/communication/#key-slack-channels), which is used solely for informal communication, you will notice that you have been automatically added to a handful of useful support channels such #questions and #it-help.

### The First Thirty Days

This issue consists of a series of tasks which should be completed sequentially over the course of the next thirty days structured as follows:

| Day | Area of Focus |
| ----- | ----- |
| **Day 01** | Accounts; Applications and Paperwork |
| **Day 02** | Remote Work; Communication and GitLab Values |
| **Day 03** | Security |
| **Day 04** | Organizational Structure and Social Elements |
| **Day 05** | Introduction to Git and using GitLab |
| **Day 06+** | Explore (Various) |

Though you may be eager to jump right into your role we encourage you to set aside dedicated time of at least one to two weeks, to focus on these tasks as they are geared toward ensuring you are enabled to [thrive in an all-remote environment](https://about.gitlab.com/company/culture/all-remote/onboarding/#the-importance-of-onboarding).

You will however see that afternoons have been allocated to role-based onboarding which can be found at the bottom of the issue above the comments section.

### Onboarding Compliance

It is important to note that certain tasks within your onboarding issue relate to compliance and ultimately keeping GitLab secure.

These tasks are marked with a red circle and completion of them is subject to audit by both the People Experience Team and various other stakeholders such as Security and Payroll so please be sure to acknowledge them once complete by checking the relevant box.

### Continuous Improvement

In the interests of paying it forward and contributing to our core values of [Collaboration](https://about.gitlab.com/handbook/values/#collaboration) and [Efficiency](https://about.gitlab.com/handbook/values/#efficiency) - we encourage you to suggest changes and updates to this issue and the handbook, both of which can be done via Merge Request (MR), which you will learn about on Day Five. The easiest way to search for anything within this issue template is to hit the `Edit` button - the small pencil icon in the top right of the issue - followed by `Command + F` which will open the search bar into which you can enter the word or phrase you are looking for.

Along with this you will close out your issue on Day Thirty by being asked to complete your [Onboarding Satisfaction Survey](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSdU8qxThxRu3tdrExFGBJ1GlyucCoJj2kDpcvOlM4AiRGOavQ/viewform), which is an opportunity to share feedback on both your general Onboarding Experience and your Onboarding Buddy Experience.

This is another opportunity for the People Experience Team to evolve and improve upon the program using the feedback provided and also allows for your Onboarding Buddy to be included in the [Quarterly Raffle](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/#buddy-program).

---

### Before Starting at GitLab

<details>
<summary>Manager Tasks</summary>

1. [ ] Select an onboarding buddy on the new team members team.  Please select someone in a similar time zone and someone that will not be on PTO the first 2 weeks. Ideally someone who has been at GitLab for at least 3 months would be helpful. It is highly encouraged that an onboarding buddy is selected for the new team member to have the best support during their onboarding. Review [Onboarding Buddies Handbook page](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/) for more information.
1. [ ] Send an email to the chosen buddy `MANAGER__PLACE__BUDDY_HANDLE__HERE` letting them know that they have been chosen, and include a link to the onboarding issue.
1. [ ] Assign the onboarding buddy to this issue by clicking Edit in the Assignees section (right-hand column) and enter the buddy GitLab handle. Please also update the onboarding `__BUDDY_HANDLE__` in the top intro section of this issue with the buddy handle by clicking edit in this issue. This will allow the onboarding buddy to have even further transparency and awareness on how the new team member is doing.
1. [ ] We encourage managers to review their schedule and ensure availability to support onboarding team members ahead of their pre-determined start date.  In an instance where you may be OOO please be sure to nominate another individual from within the department for this purpose.
1. [ ] The default laptop for the new team member will be an Apple MacBook Pro. If the new team member could benefit from using a Dell with Linux instead, remind the new team member to request it. Details and the appropriate information on laptops are included in the "Welcome" email sent to the new team member from the CES team after the team member's offer is signed, but many are overwhelmed with paperwork and new job excitement, so a friendly reminder might be in order. Refer to the handbook for more information on [approved laptops](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/#laptop-configurations).
1. [ ] Schedule a Zoom video call with the new team member for the start of their first day to welcome them to the team and set expectations. Send them an invitation to their personal email. The team members GitLab email address will only be activated on their first day.
1. [ ] Send an email to the new team member's personal email address welcoming them to GitLab. Provide them with the time and Zoom link to the introduction video call as well as any other information the manager feels will help with a great first day experience.
1. [ ] Read through the [Building Trust Handbook page](https://about.gitlab.com/handbook/people-group/learning-and-development/building-trust/). All people leaders have a responsibility to get to know their people to understand how someone would like to receive feedback.
1. [ ] Organize a smooth onboarding plan with clear starting tasks and pathway for new team member.
1. [ ] Review [GitLab's Probation Period Process](https://about.gitlab.com/handbook/people-group/contracts-probation-periods/) and check if your new team member falls within a country that has a probation period. This is important as the People Experience team will be following up with you to review the probation period, within the period on the linked page. <!-- Intern: Remove -->
1. [ ] Review any Job-specific tasks add at the bottom of this issue. Complete any manager tasks and coordinate with the People Experience Team if any tasks need to be updated.
1. [ ] If this team member will be a People Manager, please note that the Interview Training and Becoming a Manager issues will be [opened](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#create-interview-training-issue) automatically, one week after their start date to make it less overwhelming their first week. <!-- Intern: Remove -->
1. [ ] Add [Manager: Pre-Start Complete](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#issue-organization) Label once you have completed all of the above tasks. [Labels](https://docs.gitlab.com/ee/user/project/labels.html) are used to track which tasks may still be incomplete for the People Experience team.

</details>

<!-- Intern: Update to Mentor -->
<details>
<summary>Buddy Tasks</summary>

1. [ ] Review your schedule, if you will be taking extended PTO (more than a few days) or going out on leave, please alert the team members manager and ask for a new onboarding buddy to be assigned.
1. [ ] Review the [Onboarding Buddies handbook page](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/) to get familiar with your role.
1. [ ] Ask Manager for the new team member's personal email by email (not in this issue). Managers are cc'd when the Candidate Experience Team sends the new team member a Welcome email to their personal email address once the agreement is signed.
1. [ ] Set a reminder to schedule a Zoom call for their first week to introduce yourself. Make sure they know that if they have questions, they can come to you. Guide them to the right parts of the handbook, and/or encourage them to ask their questions of the wider group in Slack, etc. Send them an invitation to both their personal email and GitLab email, as it is possible that they will not have created their Zoom account linked to their GitLab email by the time of this scheduled meeting.
1. [ ] Schedule at least one follow-up chat on their second week. During that chat, offer to help them with updating the team page when they hit that task on the onboarding. Check with the new team member if they will need help setting up their SSH key and installing a Shell ready for using Git locally. You may need to set up an additional meeting for this step.
1. [ ] Check in with the new team member and see if they need help scheduling any coffee chats.
1. [ ] Consider iterating on the [onboarding issue template](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/blob/master/.gitlab/issue_templates/onboarding.md) or suggest improvements or give feedback on the #peopleops slack channel.

</details>

<details>
<summary>People Experience Tasks</summary>

**Issue Review**
   1. [ ] Assign this issue to the People Experience team member and the hiring manager (this should be done automatically, but worth checking) and check that the following information is accurate:
| BAMBOOHR_ID | `__BAMBOO_ID__` |
| MANAGER | `__MANAGER_HANDLE__` |
| PEOPLEOPS EXPERIENCE SPECIALIST | `__PEOPLE_EXPERIENCE_HANDLE__` |

**Bamboo HR**
   1. [ ] Check if new team member was referred and by whom in [Greenhouse](https://app2.greenhouse.io/dashboard). If it's not marked at the top of their Greenhouse profile, go to "Application" on the lefthand sidebar under the job they were hired for, then click "View Job Post" and look in their application questions to see if they mentioned a referrer. Make sure the source for the candidate is labeled as referred and by the appropriate person.
   1. [ ] If the new team member was referred, add a note under "Notes" in the team member's BambooHR profile with the name of who referred them. Also [add the referral bonus](https://about.gitlab.com/handbook/incentives/#referral-bonuses) for the referrer in BambooHR. Please verify based on the handbook what the amount should be depending on if the hire is in a low location area. In terms of determining if they are part of an [underrepresented group](https://about.gitlab.com/handbook/incentives/#add-on-bonus-for-select-underrepresented-groups), please follow the relevant task on Day 1.
   1. [ ] If the new team member listed any exceptions to the IP agreement in their contract, choose `Yes` in the Exception to the IP agreement field in the Jobs tab. Email the VP of Engineering, VP of Product and Legal - Employment with listed exceptions for approval. Add a pdf of the email approval thread in the Contracts & Changes folder. Set the uploaded document to "shared" so that the team member can also view it.
   1. [ ] If applicable, add a Time Off Employee Accrual Policy for team members located in GitLab Ltd (UK), GitLab GmbH (Germany), GitLab BV (Belgium), GitLab Canada Corp (Canada), GitLab GK (Japan), GitLab Inc (China), and GitLab BV (Netherlands). Click on "Time Off", hover your cursor over the "Employee Accruals" box, and you should see "Accrual Options" appear at the bottom. Click on it and select the appropriate policy from the dropdown. Click Save.

**Communication**
1. [ ] Check to see whether the manager is or is planning any PTO during the first week of onboarding. Contact an alternative team member to assist if this is the case.
1. [ ] Add the following comment to the issue and tag the manager.
> I have created the onboarding issue; inside this issue, there are tasks for you and the onboarding buddy (you will assign). A portion of those tasks must be completed before the start date to ensure a successful onboarding process. We also suggest allowing for the new team member to have two full weeks to focus only on this onboarding issue. Please let me know if you have any questions.
1. [ ] Notify the manager of the new team members GitLab email address, don't forget to mention that the email account will only be activated on the team members start date (this will avoid any confusion with returning mail delivery failures).
1. [ ] Review the Onboarding Buddy tasks have been completed, if not send a friendly ping in this issue.
1. [ ] Review the Manager Pre-Start tasks have been completed, if not send a friendly ping in slack.

**Last Steps**
These steps need to happen 1 day prior to the new hire's start date. This will ensure the `BambooHR <> Sync` has taken place and the email address has been created.

1. [ ] The following steps are crucial for the new hire to be able to access this onboarding issue:
   1. [ ] Check that an invitation has been to the new team for [gitlab-com group](https://gitlab.com/groups/gitlab-com/-/group_members) as a `Developer`. Note that `Guest` privilege is **not** enough.
   1. [ ] Check that an invitation has been to the new team for [gitlab-org group](https://gitlab.com/groups/gitlab-org/-/group_members) as a `Developer`. Note that `Guest` privilege is **not** enough.
1. [ ] Log into [1Password](https://gitlab.1password.com/people) and resend an invite to the new team members GitLab email address.
1. [ ] Request signature of Code of Conduct 2021 from BambooHR following the relevant task list in the [Day 1 Onboarding Tasks](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#day-1-onboarding-tasks) section of the Onboarding Processes handbook page.
1. [ ] Request signature of Acknowledgement of Relocation 2021 from BambooHR following the relevant task list in the [Day 1 Onboarding Tasks](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#day-1-onboarding-tasks) section of the Onboarding Processes handbook page.
1. [ ] Add [PE: Pre-Start Complete](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#issue-organization) Label once you have completed all of the above tasks.

</details>

---

### Accounts and Access

<details>
<summary>People Experience Tasks</summary>

#### Day 1

**Calendars & Agenda**
   1. [ ] Add new team member to the next quarterly [CEO 101 call](https://about.gitlab.com/culture/gitlab-101/) in the GitLab Team Meetings calendar for EMEA/US team member. Also invite new team member to the "CEO 101 Introductions" meeting that takes place right before it. When prompted to "Edit recurring event", choose "This event".
      - If the new team member is in APAC, please add to the APAC CEO AMA. When prompted to "Edit recurring event", choose "This event".
   1. [ ] Invite the new team member to the next Onboarding Office Hours. Ensure to select the meeting time zone applicable to the new team member (APAC/EMEA/US) When prompted to "edit recurring event", choose "this event".
1. [ ] Add new user as a Manager to the [GitLab Unfiltered YouTube account](https://myaccount.google.com/u/3/brandaccounts/103404719100215843993/view?pli=1) by clicking "Manage Permissions" and entering their GitLab email address.
1. [ ] If the team member was referred, review their self-identification data in BambooHR, specifically the Gender, Ethnicity and Veteran status. If any of these place the team member in an [underrepresented group](https://about.gitlab.com/handbook/incentives/#add-on-bonus-for-select-underrepresented-groups) please update their referrer's Referral bonus amount.
1. [ ] Add [PE: Day 1- Complete Label](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#issue-organization) once you have completed all of the above tasks.

#### Compliance
**After Day 1**
1. [ ] Verify that the new team member has checked the New Hire Security Orientation video training task (Day 1 Morning - Security Tasks).
1. [ ] Verify that the new team member has checked the Survey Security form-related task and submitted a response to the form (Day 1 Morning - Security Tasks).
1. [ ] Verify that the new team member has acknowledged and signed the `Code of Conduct 2021` and `Acknowledgement of Relocation 2021` in BambooHR. You would have received an email to notify you that this has been signed.

**After Day 2**
1. [ ] Move the scanned photo of ID to the Verification Folder in BambooHR. Important: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR. Update BambooHR if applicable. This is important in order for Total Rewards to complete their audit and remain compliant. Please ping the team member if the ID / passport has still not been uploaded by the end of Day 2.

**After Day 3**
1. [ ] Check to ensure encryption screenshot has been uploaded and an encryption label (`encryption_missing` or `encryption_complete`) has been added to the issue. If it hasn't been added, please ping the team member in the onboarding issue to please complete the task.

**After One Week**
1. [ ] Add a comment to this issue, checking in on the new team member. Check on their progress within the issue.
1. [ ] If the new team member is a Manager, ensure that the `Becoming a Manager` and `Interview Training` issues have been opened and automatically assigned to the team member.

**After 4 Weeks**
1. [ ] Check that the team member has reviewed the handbook by completing Task Number 2 "Read the team handbook".
1. [ ] If the team member has completed the majority or all tasks in the issue, make a comment to congratulate the team member for completing their onboarding and that you will be closing the issue.

</details>

<details>
<summary>Total Rewards Tasks</summary>

#### Day 1
1. [ ] Total Rewards Analyst:
    * In BambooHR, update access levels for Managers and Contractors, if applicable, as follows:
        * For Employees who are Managers of people: "Managers"
        * For Contractors (independent or corp-to-corp): "Contractor Self-Service"
        * For Contractors who are Managers of people: "Multiple Access Levels": "Contractor Self-Service" and "Managers"
    * In BambooHR, audit the entry and mark complete in the "Payroll Change Report".

</details>

<details>
<summary>Manager Tasks</summary>

#### Day 1

1. [ ] Invite team member to recurring team meetings, consider pressing `don't send`, this will reduce noise in their inbox on day 1.
1. [ ] Schedule weekly [1:1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with new team member. Use the first 15 minutes to get to know your new team member on a personal level.
1. [ ] Set new GitLab team member's GitLab group and project-level permissions as needed.
1. [ ] Ensure new GitLab team member knows which individuals they will be collaborating with and working with on a day-to-day basis in their functional groups.
1. [ ] Introduce your new team member to your team at your next team call. Ask them to tell the team about where they were before GitLab, why they wanted to join the team, where they are located, and what they like to do for fun outside of work.
1. [ ] Add [Day 1- Complete Label](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#issue-organization) once you have completed all of the above tasks.

#### Day 2

1. [ ] A [role based entitlement Access Request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/baseline-entitlements/#role-entitlements-for-a-specific-job) will be created automatically for new team members **if** a [pre-approved template exists](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/tree/master/.gitlab/issue_templates/role_baseline_access_request_tasks) and you will be tagged in it automatically (starting 2020-Apr). The AR will also be automatically added to the new team member epic together with their onboarding issue. The AR should request access to anything NOT provided by [baseline entitlements for all GitLab team members](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/baseline-entitlements/#baseline-entitlements-all-gitlab-team-members).
    - As the manager, please check if the AR covers the correct entitlements.
    - If the AR was not created AND a role baseline entitlement exists for the role, please create an [issue for People Ops Engineering](https://gitlab.com/gitlab-com/people-group/people-operations-engineering/-/issues/new)  and proceed with manually creating the AR for the new team member. If creating it manually, don't forget to include the [person's details](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request.md) in the issue.
    - For all other roles that do not have a role-specific AR template, open a [Individual or Bulk Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request). We recommend [creating a role based entitlements template](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/baseline-entitlements/#how-do-i-create-a-role-based-entitlement-template) so all future team members with a role get the AR created automatically.

</details>

<details>
<summary>Accounts Payable Tasks</summary>

#### Day 1

1. [ ] Accounts Payable (@Courtney_cote @Ngkhan): If an applicable policy exists for the employee type and entity, [invite team member](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#add-expensify) to an account in [Expensify](https://www.expensify.com/signin). If contractor, search to see if there is an `iiPay` policy in the currency of the team member's compensation, and invite to that policy if it exists. If none currently exists, create a new policy and the new team member to that policy.

</details>

<details>
<summary>Legal Tasks</summary>

#### Day 1

1. [ ] Legal (@tnix): Add team member to NAVEX LMS

</details>

---

<!-- include: country_tasks -->

---

<!-- include: entity_tasks -->

---

### Day 1 - Getting Started: Accounts and Paperwork
<!-- I was wondering if it was possible to change "New Team Member" to the team member's name? Is there a way to automate that with a variable somehow? That way they know EXACTLY what's for them and what isn't for them. for example, instead of "New Team Member" it would  say "Jared" or "Sonya". I was a little confused at first. I quickly understoon what was for me and what wasn't, but it was a little friction in the process-->
<details>
<summary>New Team Member</summary>

1. [ ] Assign the onboarding issue to yourself by adding yourself to the current team members currently assigned by clicking `Edit` in the Assignees section (right-hand column) and enter your GitLab handle.
1. [ ] Take a look at our Onboarding Feedback [page](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-feedback/) and see what other team members have shared about their onboarding experience.
1. [ ] If you are already in possession of a company owned Apple laptop for use in your new role at Gitlab, please create or update the Apple ID, using your GitLab email address. If you have [problems creating an apple ID from system preferences](https://discussions.apple.com/thread/8438000), try creating it through the [App Store](https://support.apple.com/en-us/HT204316).  If an app is critical to your work, it can be reimbursed through Expensify. If your laptop is of a different make, please assign any computer or store ID to your GitLab email address.
##### Security Tasks
1. [ ] :red_circle: Watch the recorded [New Hire Security Orientation](https://www.youtube.com/watch?v=kNc9f-LH6pU) training.
1. [ ] :red_circle: Familiarize yourself with the process to [engage security on-call](https://about.gitlab.com/handbook/engineering/security/#engaging-the-security-on-call)
1. [ ] :red_circle: Complete the [survey/feedback](https://docs.google.com/forms/d/1_jUbA961Mj44paTfiI2VFTMsElAdjEqFsLrfPKfoW04) form for the Security Orientation training.
1. [ ] :red_circle: Read our [Security Practices](https://about.gitlab.com/handbook/security) page, especially our Password Policy Guidelines.
1. [ ] :red_circle: Jamf enrollment - *Mac only*
    - If you have self-procured your machine, please make sure to [enroll in Jamf](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/endpoint-management/jamf/#enrolling-in-jamf).
    - If the company has procured the machine for you, please confirm that [Jamf is installed correctly](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/endpoint-management/jamf/#installation-completion-confirmation). If not, please follow the steps to [enroll in Jamf](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/endpoint-management/jamf/#enrolling-in-jamf). Please ask any questions in the `#it_help` Slack channel.

##### 1Password
1. [ ] :red_circle: Install 1Password. GitLab provides all team members with an account to 1Password. Some teams use 1Password for shared notes and some shared accounts. 1Password is available as a standalone application for macOS, and as a browser extension and CLI tool for Linux.
   1. [ ] The 1Password invitation email will be in your GitLab email inbox, please search for it and accept the invitation.
   1. [ ] Install the 1Password app or browser extension on your computer, and link it to your team (GitLab company) account, as described on the [security practices page](https://about.gitlab.com/handbook/security/#adding-the-gitlab-team-to-a-1password-app). Tip: Please use Google Chrome on your laptop as our applications are setup in Google environments. Using Safari (Apple users) can cause some issues with setup. Please let People Experience or your manager know if you need any assistance.
   1. [ ] Read 1Password's [getting started guides](https://support.1password.com/explore/extension/) and the [Mac app's guide](https://support.1password.com/getting-started-mac/) to understand its functionality.
   1. [ ] Ensure that your 1Password master key is unique and random. Start learning it!
   1. [ ] Before saving passwords in 1Password, understand why some will be in 1Password versus Okta. For passwords that cannot be saved in Okta, remember to save your passwords in your [*Private* vault](https://support.1password.com/1password-com-items/) (only accessible to you) rather than a shared vault (accessible to others at GitLab).
   1. [ ] Create a secure password for your laptop lock screen.
##### Okta
1. [ ] :red_circle: Set up your Okta Account. GitLab uses Okta as its primary portal for all SaaS Applications, and you should already have an activation email in both your Gmail and Personal Accounts. Okta will populate your Dashboard with many of the Applications you will need, but some of these will require activation. Read and Review the [Okta Handbook](https://about.gitlab.com/handbook/business-technology/okta/) page for more information.
   1. [ ] Tip: Please use Google Chrome on your laptop as our applications are setup in Google environments. Using Safari (Apple users) can cause some issues with setup.
   1. [ ] Reset your GitLab password via Okta. Make sure that you start from the [Okta dashboard](https://gitlab.okta.com/app/UserHome) to log into your GitLab account. Do this by clicking the GitLab.com tile directly from your Okta dashboard.
   1. [ ] Reset your Google password via Okta and set up 2FA for your Google account immediately.
   1. [ ] If you created any accounts while onboarding before being added to Okta, add these passwords to the relevant Okta Application.
   1. [ ] Reset any additional existing passwords in line with the security training & guidelines.
   1. [ ] Confirm in the comment box that you are using 1Password and Okta in accordance with our Security Practices.
   ##### 2FA (Two-Factor Authentication)
1. [ ] GitLab requires you to enable 2FA (2-Step Verification, also known as two-factor authentication), because it adds an extra layer of security to your account. You sign in with something you know (your password) and something you have (a code you can copy from your Virtual Multi-Factor authentication Device, like FreeOTP, 1Password or Google Authenticator). Please make sure that time is set automatically on your device (ie. on Android: "Settings" > "Date & Time" > "Set automatically"). If you have problems with 2FA just let IT Ops know in the #it_help Slack channel! We can disable it for you to then set it up again. If the problem persists, then we can direct you to one of our super admins. Reach out proactively if you're struggling - it's better than getting locked out and needing to wait for IT Ops to help you get back online.
1. [ ] :red_circle: Do the next 3 steps Today. Actually -- why not RIGHT NOW. This absolutely must be done within 48 hours or you will be locked out and need to wait for IT Ops to get around to helping you get reconnected.
   1. [ ] :red_circle: Enable 2FA on your GitLab email account (Gmail/GSuite) (this should have been an option to do when you first received the invitation to the account). Google does not support 2FA in some countries ([e.g. Nigeria](https://productforums.google.com/forum/#!topic/gmail/3_wcd8tAqdc)); if that is the case for you, reach out to IT Ops in the #it_help Slack channel to get the initial authentication code sent to a phone in a supported country, after which 2FA will work as normal. Please comment in this issue when this is completed.
   1. [ ] :red_circle: If you authenticated your GitLab.com account with Google, GitHub, etc. you should either disconnect them or make sure they use two-factor authentication.
   1. [ ] :red_circle: Enable [two-factor authentication](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html) on your GitLab.com account.

##### BambooHR
***This section is super important to ensure we can continue our tasks in the People Group and Payroll team. Please complete these items on your first day if at all possible.***
1. [ ] BambooHR is our HRIS (Human Resource Information System) for all team members. We have self-service enabled so that at any time you can access your account to see your employment information and documentation. As part of onboarding, please make sure to update everything that is applicable to you. The information from BambooHR is used to feed other systems, so it is important that it is filled out before you start. **For US Based employees, this information will also need to be completed for benefits enrollment. Our Total Rewards team usually enrolls new team members in all benefits on day 4 of onboarding (and roll back to ensure you are covered from day 1). If you are not enrolled by day 4, kindly send a slack message in the #total-rewards slack channel.**
   1. [ ] Access BambooHR from your Okta account dashboard.
   1. [ ] :red_circle: Please upload a scanned photo of your ID/Passport and Work Permit/Visa in BambooHR (My Info => Documents => Employee Uploads) by the latest of day 2 of your onboarding. If your ID documents have already been uploaded by People Experience (check the Verification Docs folder), please disregard this task. If no document(s) are there, please upload and notify People Experience by commenting at the bottom of this issue, tagging your People Experience Associate assigned to your issue by entering their GitLab handle and your message. Rest assured that this is only viewable to People Team Admins. Please note that completing this task late may result in the late processing of your first pay.
   1. [ ] On the Personal tab of the My Info page, enter your:
      1. [ ] :red_circle: Birth Date: This field is **mandatory** and is **confidential**, in order for Total Rewards team to complete employee verification and compliance. Rest assured that this is only viewable to People Team Admins. Please ensure your birth date has been filled out in the correct format. Incorrect details may result in the late processing of your first pay.
         - Once you have entered the date of birth, the field will grey out and can no longer be edited. Not to worry, a notification is sent to our Total Rewards team to approve the update. Once approved, you will be able to view your date of birth field in BambooHR.
      1. [ ] Name fields (representing Legal First Name, Legal Middle Name, Legal Last Name): Please confirm (and update if necessary) that these fields have been filled correctly with your complete Legal First, Middle and Last Name(s), as seen on any Government-issued ID.
      1. [ ] Preferred Name: Only fill out this field if you prefer to be known by a First Name other than your Legal First Name. Please leave the field blank if there is no change. Ex. If your Legal First Name is Emily but you prefer to be known as Emma, please enter Emma in the Preferred Name field.
      1. [ ] Preferred Last Name: Only fill out this field if you prefer to be known by a Last Name other than your Legal Last name. Please leave the field blank if there is no change. Ex. If your Legal Last name is Rodrigo de Leon Garcia but you prefer to be known with the last name Garcia, please enter Garcia in the Preferred Last Name field.
      1. [ ] National Identification Number in your current country of residence. If in the US, please format this as ###-##-####.
      1. [ ] Gender
      1. [ ] Other Gender Options (if applicable) (You can read more in our [Gender and Sexual Orientation Identity Definitions and FAQ](https://about.gitlab.com/handbook/people-group/gender-pronouns/) handbook page.)
      1. [ ] Disability Status (Please read more about our [Disability Inclusion](https://about.gitlab.com/company/culture/inclusion/#disability-inclusion) in our handbook.)
      1. [ ] Marital Status
      1. [ ] Address
      1. [ ] Nationality
      1. [ ] Phone numbers (make sure to add the country code, i.e. all numbers should start with `+`)
      1. [ ] :red_circle: GitLab Username (this is the username you choose when setting up GitLab, not your GitLab email address) (**NB: Please copy this without the `@` symbol**)
      1. [ ] Family Members at GitLab (if applicable)
   1. [ ] Still on the My Info page, click Export Name/Location to Team Page. This task enables you to be added to our [team page](https://about.gitlab.com/company/team/). We are highly sensitive and inclusive to privacy, we therefore respect your decision to remain private/anonymous if you prefer. To add your entry to the [team page](https://about.gitlab.com/company/team/), please update the BambooHR field to "YES". This will trigger the entry automatically. If no action is taken or you enter "NO", an anonymized entry will be created. The intention behind the team page entries relates to our [Transparency value](https://about.gitlab.com/handbook/values/#transparency).
   1. [ ] On the Job tab, enter your ethnicity. If you would prefer not to answer, please leave it blank.
   1. [ ] On the Emergency tab, enter in all emergency contact information.
1. [ ] On the More tab, click Bank Information and fill in your bank details (Not applicable to USA, Safeguard, CXC & Global Upside team members).


##### Acknowledgements
1. [ ] :red_circle: Review and sign the [Code of Conduct 2021](https://about.gitlab.com/handbook/legal/gitlab-code-of-business-conduct-and-ethics/). This document was sent to your GitLab email inbox from BambooHR requesting your signature. Please comment in this issue and ping your assigned People Experience team member if you have not received it.
1. [ ] :red_circle: Review and sign the `Acknowledgement of Relocation 2021`. This document was sent to your GitLab email inbox from BambooHR requesting your signature. Please comment in this issue and ping your assigned People Experience team member if you have not received it.
1. [ ] If you are not already offered a GitLab life insurance plan through the entity-specific benefits, fill in the [expression of wishes form](https://docs.google.com/document/d/1bBX6Mn5JhYuQpCXgM4mkx1BbTit59l0hD2WQiY7Or9E/edit?usp=sharing). For your nominees to have access to the benefit, you must indicate in writing to whom the money should be transferred. To do this, first copy the template to your Google Drive (File -> Make a copy), enter your information, sign electronically. To sign the document, use a free document signing program like [smallpdf](https://smallpdf.com/sign-pdf) or [HelloSign](https://app.hellosign.com/); or you can print it, sign and digitize. Sign, save as a PDF and upload to the **Employee Uploads** folder in BambooHR.
     - On a Mac you can electronically sign via exporting the document to PDF and then using [Preview](https://support.apple.com/guide/preview/fill-out-and-sign-pdf-forms-prvw35725/mac).
     - On Linux, you can [use LibreOffice and GnuPG to create a certificate and sign the PDF](https://www.techrepublic.com/article/how-to-sign-libreoffice-6-documents-with-gnupg/).

1. [ ] Take the [GitLab Compensation Knowledge Assessment](https://about.gitlab.com/handbook/total-rewards/compensation/#knowledge-assessment) along with the [GitLab Benefits Knowledge Assessment](https://about.gitlab.com/handbook/total-rewards/benefits/#knowledge-assessment) and the[GitLab Equity Knowledge Assessment](https://about.gitlab.com/handbook/stock-options/#equity-knowledge-assessment), remember any questions you may have around these can be routed to the #total-rewards Slack channel.
1. [ ] Check out the [People Operations handbook page](https://about.gitlab.com/handbook/people-group/) and learn more about the People team and everything under our purview.

##### Slack
1. [ ] Using your @gitlab.com email, register on Slack by following this [invitation link](https://join.slack.com/t/gitlab/signup). Read the next suggestions on how to choose a username first.
1. [ ] Pick your [Slack username](https://gitlab.slack.com/account/settings#username) to be the same as your GitLab email handle, for consistency and ease of use.
1. [ ] Fill in your [Slack profile](https://gitlab.slack.com/account/profile), as we use Slack profiles as our Team Directory to stay in touch with other team members. The fields include:
    1. [ ] Photo 
    1. [ ] What I Do (can be your job title or something informative about your role)
    1. [ ] Phone Number including country code
    1. [ ] Time Zone (useful for other GitLab team members to see when you're available)
    1. [ ] GitLab.com profile, set the Display text to your `@gitlabusername`
    1. [ ] Job Description link from our handbook
    1. [ ] City and country
    1. [ ] Working hours, can help others to identify the times you are generally available
    1. [ ] GitLab Birthdays: Opt in or out of the birthday celebrations by selecting `yes` or `no`
    1. [ ] If you'd like to, you can add your personal email address instead of your GitLab email address (this is optional)
    1. [ ] Consider changing your ["display name"](https://get.slack.help/hc/en-us/articles/216360827-Change-your-display-name) if you prefer to be addressed by a nickname
    1. [ ] Consider creating and adding your own [README](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/website/#creating-and-publishing-your-gitlab-readme). This may take some time to create so please feel free to return to this task and complete it at a later date.
    1. [ ] At GitLab [Transparency](https://about.gitlab.com/handbook/values/#transparency) is a value and we prefer to use public channels in Slack. Continue to use public channels wherever possible, especially if it is a work related discussion.
1. [ ] Read our handbook section on [communication via Slack](https://about.gitlab.com/handbook/communication/#slack), paying particular attention to the item concerning `Please avoid using @here or @channel unless this is about something urgent and important`.
1. [ ] Introduce yourself in the Slack [#new_team_members](https://gitlab.slack.com/messages/new_team_members/) channel, where you can ask any questions you have and get to know other new team members! Tips:
   1. Make sure your Slack profile has a **photo** - it makes it easier for other GitLab team members to remember you! 
   1. We love to hear more, such as where you were before, family/pets, and hobbies. 
   1. We also enjoy pictures if you're willing to share. Consider giving your new team members a glimpse into your world (scroll through previous messages in the channel for inspiration).
1. [ ] Complete the 8 short Slack lessons found at [Slack 101](https://slack.com/resources/slack-101).
1. [ ] Check out some of GitLab's [key Slack channels](https://about.gitlab.com/handbook/communication/#key-slack-channels) to get started. You can also reference the Slack help center for [instructions on browsing all available channels](https://slack.com/help/articles/205239967-Join-a-channel); we have thousands!

##### Additional Applications to Note
- [ ] We use TripActions for company travel; please [create your account through Okta](https://gitlab.okta.com/app/UserHome). Head to the [travel page](https://about.gitlab.com/handbook/travel/#setting-up-your-tripactions-account) for further instructions on how to setup your account.
- [ ] Keep track of your expenses in Expensify (via Okta). If you have any questions on submitting an expense report ask in the `#expense-reporting-inquires` slack channel.
  - Opt-out from the Expensify newsletter in your contact preferences.

</details>

---

### Day 2 - Remote Working and our Values

<details>
<summary>New Team Member</summary>

### Remote Work
1. [ ] The first month at a remote company can be hard. **There is as much to unlearn as there is to learn**. Throughout your first few weeks, you will start the GitLab [Remote Work Foundations learning certification](https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge). This morning start the first two assessments of 10. These two lessons will set you up for success during your first few days at GitLab. You can view the entire curse listed below.
1. [ ] Particularly if you're new to working in a 100% remote environment, read over our [Guide for starting a new remote role](https://about.gitlab.com/company/culture/all-remote/getting-started/). You'll find other tips for operating (and thriving!) in an all-remote setting within the [All-Remote section of our handbook](https://about.gitlab.com/company/culture/all-remote/). Take some inspiration from our team members on how they structure their remote working day, read this helpful blog post [A day in the life of remote worker](https://about.gitlab.com/blog/2019/06/18/day-in-the-life-remote-worker/)
1. - [ ] Learn more about the `#allremote` onboarding process and additional [best practices](https://about.gitlab.com/company/culture/all-remote/learning-and-development/#how-do-you-onboard-new-team-members).
1. [ ] Complete the Remote Work Foundations Certification
    - [Handbook First Knowledge Assessment](https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge) (Day 1)
    - [Adopting a Self-Service and Self-Learning Mentality Knowledge Assessment](https://about.gitlab.com/company/culture/all-remote/self-service/#gitlab-knowledge-assessment-adopting-a-self-service-and-self-learning-mentality) (Day 1)
    - [Informal Communication in an All-Remote Environment Knowledge Assessment](https://about.gitlab.com/company/culture/all-remote/informal-communication/#gitlab-knowledge-assessment-informal-communication-in-an-all-remote-environment)
    - [Embracing Asynchronous Communication Knowledge Assessment](https://about.gitlab.com/company/culture/all-remote/asynchronous/#gitlab-knowledge-assessment-embracing-asynchronous-communication)
    - [ ] [All-Remote Meetings Knowledge Assessment](https://about.gitlab.com/company/culture/all-remote/meetings/#gitlab-knowledge-assessment-all-remote-meetings)
    -  [Communicating Effectively and Responsibly Through Text Knowledge Assessment](https://about.gitlab.com/company/culture/all-remote/effective-communication/#gitlab-knowledge-assessment-communicating-effectively-and-responsibly-through-text)
    - [ ] [Building and Reinforcing a Sustainable Culture Knowledge Assessment](https://about.gitlab.com/company/culture/all-remote/building-culture/#gitlab-knowledge-assessment-building-and-reinforcing-a-sustainable-culture)
    - [ ] [Combating Burnout, Isolation, and Anxiety in the Remote Workplace Knowledge Assessment](https://about.gitlab.com/company/culture/all-remote/mental-health/#gitlab-knowledge-assessment-combating-burnout-isolation-and-anxiety-in-the-remote-workplace)
    - [ ] [Remote Collaboration and Whiteboarding Knowledge Assessment](https://about.gitlab.com/company/culture/all-remote/collaboration-and-whiteboarding/#gitlab-knowledge-assessment-remote-collaboration-and-whiteboarding)
    - [ ] [The Non-Linear Workday Knowledge Assessment](https://about.gitlab.com/company/culture/all-remote/non-linear-workday/#gitlab-knowledge-assessment-non-linear-workday)

### Values
1. [ ] GitLab values are a living document. In many instances, they have been documented, refined, and revised based on lessons learned (and scars earned) in the course of doing business. Familiarize yourself with our [Values page](https://about.gitlab.com/handbook/values/).
   1. [ ] Take and pass our [Values Certification](https://gitlab.edcast.com/pathways/gitlab-values-badge) to become certified in our GitLab Values.
1. [ ] Please read the [anti-harassment policy](https://about.gitlab.com/handbook/anti-harassment/) and in particular any country specific requirements that relate to your location.  
1. [ ] Please complete the [Harassment Prevention Training](https://about.gitlab.com/handbook/people-group/learning-and-development/#compliance-courses) via WILL Learning within your first 30 days. Instructions can be found under the *Common Ground: Harassment Prevention Training* section. You can find the link to the appropriate course in BambooHR (for detailed instructions [see here](https://about.gitlab.com/handbook/people-group/learning-and-development/compliance-courses/#will-interactive)).
   1. [ ] Ensure you upload your results into BambooHR, following the above instructions.
1. [ ] Please read our [People Policy](https://about.gitlab.com/handbook/people-group/people-policy-directory/) directory which contains important team member-related policies to ensure that we adhere with all required laws.  
1. [ ] Please review and read any [Labor and Employment Notices](https://about.gitlab.com/handbook/labor-and-employment-notices/) that apply to your location. 

### Zoom
1. [ ] Set up your [Zoom account](https://about.gitlab.com/handbook/tools-and-tips/zoom/#zoom-setup). Your Zoom account will be set up via [Okta for SSO Login](https://gitlab.okta.com/app/UserHome), so login using the Zoom SSO tile.
   1. [ ] The Zoom app also allows login via Okta as SSO. In order to test this, first log out, then select `Sign in with SSO` and use `gitlab.zoom.us` as URL. Log in to Okta and proceed.
   1. [ ] Fill in your [profile](https://zoom.us/profile) with your Name, Job Title and Profile Picture. Since Zoom's job title field isn't displayed during calls, please add your job title as part of your last name in the last name field. For example, if your name is `John Appleseed` and your role is `Engineer`, you can write first name: `John` and last name: `Appleseed - Engineer`.
   1. Ensure that your Display Name is your name and job title again. Following from the example from above, your Display Name should be `John Appleseed - Engineer`
   1. [ ] Consider setting your default recording view to "Gallery view". To do this:
      1. Go to zoom.us and log into [your profile](https://zoom.us/profile).
      1. Click the `Settings` tab on the left, then the `Recording` tab.
      1. Make sure you have `Record gallery view with shared screen` selected, and unselect `Record active speaker with shared screen` and `Record active speaker, gallery view and shared screen separately`. Remember to save.
   1. [ ] Set up the Zoom Scheduler plugin for [Chrome](https://chrome.google.com/webstore/detail/zoom-scheduler/kgjfgplpablkjnlkjmjdecgdpfankdle/related?hl=en) or [Firefox](https://addons.mozilla.org/en-US/firefox/addon/zoom-new-scheduler/) - this will allow you to automatically add Zoom links to Google Calendar events.
   1. [ ] Or, if you're browser agnostic and you'd prefer not to use a plugin, there's [Zoom for Google Calendar](https://gsuite.google.com/marketplace/app/zoom_for_google_calendar/364750910244). Using the app integration you'll have video conferencing info natively in your event, rather than text/links in the description.

### Calendar

#### Google Calendar Set Up
1. [ ] Read our handbook section on [Google Calendar](https://about.gitlab.com/handbook/tools-and-tips/#google-calendar)
1. [ ] Set your Google Calendar [default event duration](https://calendar.google.com/calendar/r/settings) to use `speedy meetings`
1. [ ] Verify you have access to view the GitLab Team Meetings calendar, and add the calendar. Go to your calendar, left sidebar, go to Other calendars, press the + sign, Subscribe to calendar, and enter in the search field `gitlab.com_6ekbk8ffqnkus3qpj9o26rqejg@group.calendar.google.com` and then press enter on your keyboard. Reach out to a People Experience Associate if you have any questions. NOTE: Please do NOT remove any meetings from this calendar or any other shared calendars, as it removes the event from everyone's calendar.
1. [ ] Consider adding our [Events and Sponsorship calendar](https://about.gitlab.com/handbook/marketing/events/#which-events-is-gitlab-already-sponsoring) to your view. This will allow you to keep up with where in the world you can connect with GitLab.
1. [ ] Consider adding the [GitLab Team Member Meetup](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV85cWZnajRuMm9nZHMydmlhMDFrb3ZoaGpub0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) calendar to your calendar.
1. [ ] Be aware your Google Calendar (tied to your GitLab Google account) is internally viewable by default. If you have private meetings often, you might want to [change this](https://support.google.com/calendar/answer/34580?co=GENIE.Platform%3DDesktop&hl=en) in your calendar settings. However, even private meetings are viewable by your manager and those who manage them.
1. [ ] Optional: [Set your picture](https://myaccount.google.com/personal-info) in Google so that it will be displayed in Google documents (vs your first initial) to show others where your cursor is in a document during a meeting.

#### Calendly
1. [ ] Set up [Calendly](https://about.gitlab.com/handbook/tools-and-tips/other-apps/#sts=Calendly). Calendly is a calendar tool that allows individuals to select open meeting slots in order to speak with others outside of GitLab. For GitLab team members, feel free to [schedule directly in Google Calendar](https://about.gitlab.com/handbook/communication/#scheduling-meetings). When you are setting up Calendly, there is no specific GitLab account. With the free version you will only be allowed one meeting time, but if you need to upgrade to a Pro account, you can do so and expense it per [spending company money](https://about.gitlab.com/handbook/spending-company-money).
1. [ ] Add the Zoom integration by going to the `Integrations` section at the top of the page. Find Zoom in the list of integrations and click it. It will then ask you to link your Calendly and Zoom accounts together. Do so.
1. [ ] Now make Zoom the default event location by editing each 'Event type' in Calendly (By default, there should be a `15 Minute Meeting`, `30 Minute Meeting` and `60 Minute Meeting` event types) by clicking the gear icon associated with each event type then going to `Edit`. Click on `What event is this?` and under `Location` choose Zoom. Click on `Save and Close`, and do this for each remaining event type.
1. [ ] Add Calendly link to your Slack Profile.
1. [ ] [Set your working hours & availability](https://support.google.com/calendar/answer/7638168?hl=en) in your Google Calendar.
1. [ ] Link your GitLab email address to an easily recognizable photo of yourself. It is company policy to use a photo, and not an avatar, a stock photo, or something with sunglasses for your GitLab accounts, as we have a lot of GitLab team members and our brains are comfortable with recognizing people; let's use them. 
1. [ ] Consider setting up an email signature. [Here is an example you can use](https://about.gitlab.com/handbook/tools-and-tips/#email-signature). Feel free to customize it how you'd like.
1. [ ] View the `Card 4.19.17.pdf` located [here](https://drive.google.com/drive/folders/0B4eFM43gu7VPUXBUa251RHFrUnM) and save your [Traveler Insurance ID card](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#business-travel-accident-policy) located on page 1.

#### Events
On your calendar, you will have a invites to the below meetings, they are optional, but you are highly encouraged to attend. Please note: These meetings may not fall during your first week.
1. [ ]  Onboarding Check-In Call. This is an Ask Me Anything (AMA) Zoom Call where you and fellow new team members can ask questions about onboarding, GitLab, or anything you need help with.
  - This event is held every other Thursday, find the event on your calendar and within the invite you should find a link to an [agenda](https://about.gitlab.com/handbook/communication/#scheduling-meetings). Feel free to add questions to this document.
1. [ ] If you think you might have questions getting your work station set up, try to sign up within 2 weeks for [Gitlab IT Onboarding/101](https://calendar.google.com/event?action=TEMPLATE&tmeid=M3VpajRiNTdmYXM3ZnViZDI1MGVnaHFvdTRfMjAxOTEyMDNUMTcwMDAwWiBtZGlzYWJhdGlub0BnaXRsYWIuY29t&tmsrc=mdisabatino%40gitlab.com&scp=ALL). This is an optional meeting for you to ask questions. We run this live troubleshooting session weekly, so you can always drop into a later meeting.
1. [ ] In the GitLab Team Calendar, look out for the Security Group Conversation, Show & Tell or Security Office Hours. All of these take place in a Zoom call where you can jump on and ask any security related questions you might have or see what new updates there are in the Security team. Please feel free to also cjoin the #security Slack channel to ask any questions you may have asynchronous.
1. [ ] [CEO 101 call](https://about.gitlab.com/culture/gitlab-101/). Within the calendar description, you will find a link to the call agenda where all team members are able to insert questions for the CEO to answer in the session. Tip: There are videos of previous CEO 101 sessions within the GitLab archives that may help you determine the question you would like to contribute.

#### Your Role

- Spend the rest of your day doing your Role-based tasks (see the Job-specific tasks at the end of this issue and in your assigned role-based onboarding issue, as applicable).

</details>

---

### Day 3 - Security & Communication

<details>
<summary>New Team Member</summary>

#### Security

1. [ ] :red_circle: Read though some of our key policies, procedures and documents from the GitLab Control Framework.
   1. [ ] [Data Classification Policy](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html)
   1. [ ] [Incident Response Guidance](https://about.gitlab.com/handbook/engineering/security/vulnerability_management/incident-response-guidance.html)
   1. [ ] [Information Security Policies](https://about.gitlab.com/handbook/engineering/security/#information-security-policies)
   1. [ ] [Security Practices, including our Password and Encryption Policies](https://about.gitlab.com/handbook/security/#best-practices)
   1. [ ] [Record Retention Policy](https://about.gitlab.com/handbook/legal/record-retention-policy/)
1. [ ] Your GitLab.com account must use your GitLab email and must only be used for GitLab related purposes, so make sure notifications are being sent to your `@gitlab.com` email.
1. [ ] Consider enabling notifications about your own activity (helpful for visibility in later Git steps). To enable this, go to Preferences -> Notifications -> Check the box before "Receive notifications about your own activity"
1. [ ] :red_circle: Encrypt your Hard Drive - instructions for both Apple and Linux machines can be found [here](https://about.gitlab.com/handbook/business-ops/team-member-enablement/onboarding-access-requests/#full-disk-encryption) with additional information around the Linux set-up process [here](https://about.gitlab.com/handbook/tools-and-tips/linux/#initial-installation).
1. [ ] :red_circle:  Leave a comment in this issue with a screenshot to verify that your hard drive is encrypted with the serial number of your computer for correlation.
   1. [ ] Please review the [Full Disk Encryption](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/#full-disk-encryption) page in the handbook for the appropriate steps to create the screenshot with the necessary information.
   1. [ ] To take a screenshot on an Apple machine press `Command` + `Shift` + `3` to take a screenshot of the entire desktop, alternativey `Command` + `Shift` + `4` will take a screenshot of the area you select. If you are using a Linux machine `Prt Scrn` will take a screenshot of the entire desktop; `Alt + Prt Scrn` will take a screenshot of a specific window and `Shift + Prt Scrn` will take a screenshot of an area you select.
   1. [ ] To Upload the screenshot to your issue: Find the `Attach a file` link at the bottom of this issue and press Comment. Depending on the operating system you are using, the steps to retrieve this information may be different.
   1. [ ] Please note that GitLab's [Internal Acceptable Use Policy](https://about.gitlab.com/handbook/people-group/acceptable-use-policy/#unacceptable-system-and-network-activities) prohibits the use of tools that capture and share screenshots to hosted third-party sites so please make sure that you do not leverage such a tool to complete this step.
1. [ ] Please add your serial code for your GitLab laptop here: [GitLab Notebook information](https://docs.google.com/forms/d/e/1FAIpQLSeUOlP11qeLdLZHTI62CFr7MSHAoI_1M6CRpnUA6fegkEKCOQ/viewform?usp=sf_link). You will need the evidence of full disk encryption from the previous step.
1. [ ] Complete [Lesson 1: Technical Terminology](https://gitlab.edcast.com/pathways/copy-of-gitlab-certification) in the GitLab Tool 101 Course. Test your knowledge with the [Technical Terminology Quiz](https://gitlab.edcast.com/pathways/copy-of-gitlab-certification)
1. [ ] Set-up and familiarize yourself with our apps: [Gmail](https://mail.google.com/), [Google Calendar](https://www.google.com/calendar/), [Slack](https://gitlab.slack.com/messages/general/) and [Google Drive](https://www.google.com/drive/) where you can [download](https://tools.google.com/dlpage/drive/index.html?hl=en) to work offline.
1. [ ] Review the [Tools and Tips section of the handbook](https://about.gitlab.com/handbook/tools-and-tips/). Many of the tools used by GitLab, including tips on how to use them, are found there.
1. [ ] Review the handbook section on [securing your data while travelling](https://about.gitlab.com/handbook/travel/#secure-your-data-during-travels), particularly if your role involves frequent travel.

#### Legal
1. [ ] Set-Up your [NAVEX Global](http://gitlab.lms.navexglobal.com) account and complete compliance training. You will be able to log into NAVEX via Okta. If you experience any issues, reach out to the team in the Slack channel `#compliance-training`.
1. [ ] Review [GitLab's internal acceptable use policy](https://about.gitlab.com/handbook/people-group/acceptable-use-policy/)
1. [ ] Don't forget to comply with the contract you signed, and make sure you understand [Intellectual Property](https://about.gitlab.com/handbook/people-group/code-of-conduct/#intellectual-property-and-protecting-ip).

#### Communication
1. Clear, considerate communication is especially important at an all-remote company. Next we will review two more assessments within the Remote Work Foundation Certificate.
   - [ ] [Embracing asynchronous communication](https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge)
   - [ ] [Communicating effectively and responsibly through text](https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge)
1. [ ] Familiarize yourself with our [Communication page](https://about.gitlab.com/handbook/communication/).
   - [ ] Take and pass our [Communication Certification](https://docs.google.com/forms/d/e/1FAIpQLScyyIQDdC3oN-H6-IJJM1QlZNHaGPI0jESb9ogAfkQlMzKgwQ/viewform) to become certified in our GitLab Communication.

#### Workspace <!-- Intern: Remove -->
- [ ] Take some time to consider the setup of your workspace. Check out these helpful Handbook pages on how to [Create an Ergonomic Workspace](https://about.gitlab.com/company/culture/all-remote/tips/#create-an-ergonomic-workspace). Having an ergonomic and comfortable workspace will enable the utmost productivity.
- [ ] Review the [Spending Company Money Guidelines](https://about.gitlab.com/handbook/spending-company-money/#guidelines) and review the [Office Equipment and Supplies Averages](https://about.gitlab.com/handbook/finance/expenses/#office-equipment-and-supplies).

#### Your Role

- Spend the rest of your day doing your Role-based tasks (see the Job-specific tasks at the end of this issue and in your assigned role-based onboarding issue, as applicable).

</details>

---

### Day 4 - Social

<details>
<summary>New Team Member</summary>

1. [ ] Review the Gitlab [Organizational Chart](https://about.gitlab.com/company/team/org-chart/) to learn how our company is structured and who reports to whom. You can also review our [Organizational Structure](https://about.gitlab.com/company/team/structure/) that gives additional information on how we structure the organization and job families.
1. [ ] Schedule at least 5 calls in your two weeks for 30 mins with at least 5 different colleagues to get to know the GitLab team. You can enter your colleagues' names below by clicking the pencil icon at the top right of this issue (under `New Issue`) and editing this section. In support of GitLab's values of [Diversity, Inclusion & Belonging](https://about.gitlab.com/company/culture/inclusion/) be mindful of scheduling calls with team members on regions other than your own - this is a great way to connect and gain insights into other walks of life. You can view GitLab team members by role and team on the [team structure chart](https://about.gitlab.com/team/chart/) and where each team member currently is in the world via the [team map](https://about.gitlab.com/company/team/). Please feel free to reach out to the People Experience team or your manager if you are unsure of who to schedule with.
   - Tip: See who engaged with your introduction in Slack in [#new_team_members](https://gitlab.slack.com/messages/new_team_members/).
   - Tip: It is generally not recommended to do all of the coffee chats in one day.
   - Tip: There's no need to ask people if it's okay to schedule a call first – just go ahead and schedule a meeting using the [Find a time](https://about.gitlab.com/handbook/tools-and-tips/#finding-a-time) feature in Google Calendar and introduce yourself in the invitation.
   - Tip: Consider joining the [Social Call](https://about.gitlab.com/handbook/communication/#social-call) as part of one of the 5 calls. This is a great way to get to know other team members across the world.
   - Tip:  Check out the [#donut-be-strangers](https://gitlab.slack.com/messages/C613ZTNEL/) channel in Slack, which will automatically set you up on one random [coffee break call](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) every other week.
   - Tip: In your [Zoom account settings](https://zoom.us/profile/setting) make sure that both "Closed Captioning" and "Live Transcription" are enabled in case they are needed.
   1. [ ] Coffee Chat with ___
   1. [ ] Coffee Chat with ___
   1. [ ] Coffee Chat with ___
   1. [ ] Coffee Chat with ___
   1. [ ] Coffee Chat with ___
1. [ ] Complete the next Remote Work Foundations Assessment: [Informal Communication in an all-remote environment](https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge)
1. [ ] Book a call with your [Recruiter](https://about.gitlab.com/handbook/hiring/recruiting-alignment/#recruiter-coordinator-and-sourcer-alignment-by-department) within four weeks of your start date. In this call, you will have the opportunity to help improve our recruiting & peopleops processes by sharing your feedback on the process so far.

#### Social Media and Connecting

1. [ ] Fill in your [GitLab.com profile](https://gitlab.com/profile), including: Organization = "GitLab", Bio = "(title)". Given our value of [transparency](https://about.gitlab.com/handbook/values/#transparency) and because GitLab is [public by default](https://about.gitlab.com/handbook/values/#public-by-default), ensure you do *not* use a [private profile](https://docs.gitlab.com/ee/user/profile/#private-profile), i.e. make sure that the checkbox under **Private profile** is unchecked.
1. [ ] Connect with or follow GitLab's social media sites: [LinkedIn](https://www.linkedin.com/company/gitlab-com), [Twitter](https://twitter.com/gitlab), [Facebook](https://www.facebook.com/gitlab), and [YouTube](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg).
1. [ ] Accept the invitation to be a Manager of the GitLab Unfiltered YouTube channel. This is super important, ***if you do not accept this invite, you will not be able to watch the recordings on Youtube Unfiltered and we use this platform daily***. If you do not see an invitation in your Inbox, please check the [Pending Invitations](https://myaccount.google.com/brandaccounts) section of your GSuite account.
    1. If you have accepted the invite you should see “GitLab Unfiltered” under the “Your Brand Accounts” section.
    1. It is important that you do not edit or change permissions of the “GitLab Unfiltered” account as this is a shared account for the whole company maintained by Marketing.
1. [ ] Confirm you have completed the above task, by watching this video (if you can't watch it, go back to the previous step): [TEST Private Stream](https://youtu.be/gCyUdHixHN0).
   1. To view GitLab Unfiltered content, open a new browser tab and navigate to [YouTube](https://www.youtube.com) and ensure that you have **switched** your account to the GitLab Unfiltered profile as detailed here: [Unable to view a video on Youtube](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube)
1. [ ] You should receive an email inviting you to Moo. Moo is the system we use to place orders for [business cards](https://about.gitlab.com/handbook/people-group/frequent-requests/#business-cards). Follow handbook directions for more detailed information.

#### Contribute

1. [ ] Check the [GitLab Contribute handbook page](https://about.gitlab.com/company/culture/contribute/) and [project page](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/blob/master/README.md) to get familiar with the GitLab Contribute event.
1. [ ] If applicable, make sure to register for the next GitLab Contribute and make travel arrangements (using TripActions) within 2 weeks of starting. You will receive an invite to do so over email.

#### Your Role

- Spend the rest of your day doing your Role-based tasks (see the Job-specific tasks at the end of this issue and in your assigned role-based onboarding issue, as applicable).

</details>

---

### Day 5 - Git

<details>
<summary>New Team Member</summary>

For those new to Git and GitLab, it's important to get familiar with the below and [bookmark these links](https://about.gitlab.com/company/culture/all-remote/self-service/) (Download bookmarks). You're not expected to become a GitLab master in a day, but to understand that the below is a key reference as you acclimate. Be sure to reach out to your onboarding buddy, manager, or any team member at GitLab for Git related questions and help.

#### Helpful Slack Channels for Git Questions
- Any help with Merge Requests: [`#mr-buddies`](https://about.gitlab.com/handbook/general-onboarding/mr-buddies/)
- Any general help with Git: [`#questions`](https://gitlab.slack.com/archives/questions)
- Specific questions using Git in the terminal: [`#git-help`](https://gitlab.slack.com/archives/git-help)

#### Introduction to Git and GitLab - SSH Access (familiar with Git)

1. [ ] If you have never used Git before, we **recommend** you skip this step and use the [WebIDE](https://docs.gitlab.com/ee/user/project/web_ide/) to complete any Git-related onboarding tasks. The WebIDE doesn't require you to set or use any command-line tools.
   * If you plan to use Git on your local machine then you'll need to [add an SSH key to your GitLab profile](https://docs.gitlab.com/ee/ssh/README.html#adding-an-ssh-key-to-your-gitlab-account). The SSH key provides security and allows you to authenticate to the GitLab remote server without supplying your username or password each time. Please note that following best practices, you should always favor ED25519 SSH keys, since they are more secure and have better performance over the other types.
   * Alternatively, you can clone over HTTPS. You'll need to generate a [personal access token](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html) as account password with 2FA enabled. This method helps when SSH is blocked in your environment (e.g. hotel wifis).

#### Introduction to Git and GitLab - General Breakdown

1. [ ] Read this informative blog post titled [GitLab 101 - A Primer for the Non-Technical](https://about.gitlab.com/2019/08/02/gitlab-for-the-non-technical/).
1. [ ] Become familiar with how GitLab works by watching the [videos for git newbies](https://about.gitlab.com/training/#git) and familiarizing yourself with other available training content.
1. [ ] Become familiar with how Gitlab works by learning our [GitLab Basics](http://doc.gitlab.com/ce/gitlab-basics/README.html).
1. [ ] Complete [Lesson 3: GitLab Merge Requests](https://gitlab.edcast.com/pathways/copy-of-gitlab-certification). Test your knowledge with this [quiz](https://gitlab.edcast.com/pathways/copy-of-gitlab-certification). It is highly encouraged to complete [lessons 1-3](https://gitlab.edcast.com/pathways/copy-of-gitlab-certification) before moving onto the **Team Page** steps.

##### Adding Yourself to the Team Page

Please be aware that this task is a key learning opportunity and strongly recommended for all team members. This exercise is an important step as a GitLab team member and is a critical first example of learning our product firsthand, in a personalized way.

1. [ ] Update your photo, social media handles and story on the team page by following the instructions in [the handbook](https://about.gitlab.com/handbook/git-page-update/#12-add-yourself-to-the-team-page). If there is anything that you would prefer to not to have added to the team page, such as your exact location, feel free to use a broad location or remove this entirely.
   * If you are a manager, your slug will need to match the `reports_to` line for the people on your team. If you update your slug you should update `reports_to` for your team members as well.
   * If you are in the Sales organization, please consider adding your Sales region.
   * Please make sure to include your nickname if you prefer to be called anything other than your first name. If you do not feel comfortable with your full name on the team page, please change it to what feels appropriate to you.
1. [ ] If you have a pet, please also consider adding them to the [team pets page](https://about.gitlab.com/team-pets/) by following the instructions on the [same page](https://about.gitlab.com/handbook/git-page-update/#12-add-yourself-to-the-team-page). Please ensure to add your first and last name and/or GitLab handle so that we can identify whose pets belong to whom.

##### Technical Git Information

1. [ ] Become aware of the [materials used by our professional services team](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/offerings/#training--education) to train our customers.
1. [ ] Become familiar with GitLab. Familiarize yourself with the dashboard, the projects, and the issue tracker. Become familiar with the `README.md`s for these projects:
   * [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/README.md)
   * [GitLab](https://gitlab.com/gitlab-org/gitlab/blob/master/README.md)
   * [GitLab FOSS Edition](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/README.md).
**Note:** that the Gitlab FOSS (Free Open Source Software) Edition is a read-only mirror of GitLab with the proprietary software removed. [This issue](https://gitlab.com/gitlab-org/gitlab/issues/13304) covers the merging of the Enterprise and Community editions.
1. [ ] Refer to [GitLab Docs](https://docs.gitlab.com/ee/README.html) for information on features of GitLab.
1. [ ] Download and [get started with Git](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html).  It is important for all team members to understand how to work both on the web interface and locally. The team is happy to assist in teaching you git.
1. [ ] Consider subscribing to [status.gitlab.com](https://status.gitlab.com) to stay aware of active incidents.

##### Issues and Issue Trackers
1. [ ] Learn how to use [issue trackers](https://docs.gitlab.com/ee/user/project/issues/). We use GitLab Issues to raise awareness, discuss, and propose solutions for various issues related to any aspect of our business. The most common issues are created in the following projects:
   * [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/issues) - Issues related to our website
   * [GitLab Project](https://gitlab.com/gitlab-org/gitlab/issues) - Customer requests and public issues related to GitLab product
1. [ ] Complete [Lesson 2: Gitlab Issues](https://gitlab.edcast.com/pathways/copy-of-gitlab-certification) in the GitLab 101 Tool. Test your knowledge with this [quiz](https://gitlab.edcast.com/pathways/copy-of-gitlab-certification).

##### Merge Requests
1. [ ] When you are first starting at GitLab and creating new merge requests, you can assign the MRs to your manager. Going forward, it's best practice to assign the MR to the relevant department; for example, if you are making an edit to the People Experience section of the handbook, please assign it to someone within People Experience. If you're unsure of who to assign a merge request to or if you have any questions or problems, feel free to reach out to your manager or onboarding buddy. When you join GitLab your GitLab.com account will be added to the [gitlab-com group](https://gitlab.com/groups/gitlab-com/-/group_members) as a Developer. Please see this [blog post discussing how we assign permissions](https://about.gitlab.com/2014/11/26/keeping-your-code-protected/), as well as a [permission levels chart](https://docs.gitlab.com/ee/user/permissions.html) to find out more about what each level can do.
1. [ ] Review best practices for issues and merge requests. It is important that we make iterations quickly and efficiently. Do not wait until you are done before creating an issue or merge request. Making issues and merge requests quickly and in small iterations allows the information to be quickly updated in the handbook and available for review by everyone. It also reduces the risk of changes becoming lost or out of date.
- `Draft:` is what you prepend to a merge request in GitLab in order to prevent your work from being merged if it still needs more work. When you are working on a big issue, create a merge request after the very first commit and give it a title, starting with `Draft:`, so you can iterate on it. For more information regarding issues, merge requests, and overall best practices regarding communication, please review [the communication guidelines](https://about.gitlab.com/handbook/communication/).
1. [ ] Review our [Practical Handbook Edit Examples](https://about.gitlab.com/handbook/practical-handbook-edits/) to learn more about creating new handbook pages and multimedia embedding best-practices, changing a page name and subsequent updates, creating mermaid diagrams, creating issue templates, among others. If you run into any issues while following those instructions, post a question in [`#mr-buddies`](https://about.gitlab.com/handbook/general-onboarding/mr-buddies/) for someone to look at and improve the documentation.
1. [ ] Make an improvement to the [handbook](https://about.gitlab.com/handbook/), [general onboarding issue](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/blob/master/.gitlab/issue_templates/onboarding.md), or your departments' onboarding issue. Here at GitLab, everyone contributes to our way of working in this way. Being new, you may be unsure if your idea for a change is good or not, and that's OK! Your merge request starts the discussion, so don't be afraid to offer your perspective as a new team member.
   - [ ] Handbook: Assign the merge request (MR) to your onboarding buddy (if they have merge rights; if not, assign to your manager), ping them in the [#new_team_members](https://gitlab.slack.com/messages/new_team_members/) slack channel, and link the MR URL in this onboarding issue. When you are making changes in the handbook, you are making changes in the www-gitlab-com project. If your manager does not have access to merge your MR based on the permissions described above, then please reference the specific [members page](https://gitlab.com/gitlab-com/www-gitlab-com/-/project_members?page=3&sort=access_level_desc) for the www-gitlab-com project.
   - [ ] Onboarding issue: If the issue template is related to your department, assign it to your manager. Otherwise, for the general onboarding issue mention your People Experience Associate (PEA) as a comment in the MR.
1. [ ] Learn more about GitLab quick actions and keyboard shortcuts for issues and merge requests in [this blog post on productive workflows](https://about.gitlab.com/blog/2021/02/18/improve-your-gitlab-productivity-with-these-10-tips/)   

#### Weeks 2-4 Reminders
- [ ] Schedule work blocks over the next 30 days on your calendar to complete the tasks under Weeks 2 - 4 below. [Focus Fridays](https://about.gitlab.com/handbook/communication/#focus-fridays) are a great time to come back to this issue.

#### Your Role

- Spend the rest of your day doing your Role-based tasks (see the Job-specific tasks at the end of this issue and in your assigned role-based onboarding issue, as applicable).

#### Happy Dance

1. [ ] Do a little happy dance. You just finished your first week at GitLab!

</details>

---

### Weeks 2 - 4 : Explore <!-- Intern: Adjust to second week -->

<details>
<summary>New Team Member</summary>

1. [ ]  Be sure to spend equal time on this critical onboarding issue, as well as your role-based onboarding and training.
1. [ ] :red_circle: Put some time aside to uncover new information in the team [handbook](https://about.gitlab.com/handbook/). A core value of GitLab is documentation. Therefore, everything that we do, we have documented in the handbook. This is NOT a traditional company handbook of rules and regulations. It is a living document that is public to the world and constantly evolving. Absolutely anyone, including you, can suggest improvements or changes to the handbook! It's [encouraged](https://about.gitlab.com/handbook/handbook-usage/)! The handbook may seem overwhelming, but don't let it scare you. To simplify navigating the handbook, here are some suggested steps. Feel free to take a wrong turn at any time to learn more about whatever you are interested in.
   - [ ] Read [about](https://about.gitlab.com/about/) the company, and [how we use GitLab to build GitLab](https://about.gitlab.com/2015/07/07/how-we-use-gitlab-to-build-gitlab/). It is important to understand our [culture](https://about.gitlab.com/culture/), and how the organization was started. If you have any questions about company products you can always check out our [features](https://about.gitlab.com/features/#compare) and [products](https://about.gitlab.com/products/).

#### Diversity and Inclusion
1. [ ] Understanding that Diversity, Inclusion & Belonging is one of GitLab's top priorities please take a look at GitLab's Diversity, Inclusion & Belonging Resources
   - [ ] GitLab's [Diversity, Inclusion & Belonging page](https://about.gitlab.com/company/culture/inclusion/)
   - [ ] GitLab encourages team member participation in [Team Member Resource Groups (TMRGs)](https://about.gitlab.com/company/culture/inclusion/erg-guide/) which include [Minorities in Tech (MIT)](https://about.gitlab.com/company/culture/inclusion/erg-minorities-in-tech/), [Women+](https://about.gitlab.com/company/culture/inclusion/tmrg-gitlab-women/) and [GitLab DiversABILITY](https://about.gitlab.com/company/culture/inclusion/erg-gitlab-diversability/) amongst [others](https://about.gitlab.com/company/culture/inclusion/erg-guide/#how-to-join-current-tmrgs-and-their-slack-channels).
   - [ ] GitLab's [Diversity, Inclusion & Belonging Events](https://about.gitlab.com/company/culture/inclusion/diversity-and-inclusion-events/)
   - [ ] GitLab's [Diversity, Inclusion & Belonging trainings](https://about.gitlab.com/company/culture/inclusion/#diversity-inclusion--belonging-training-and-learning-opportunities)
   - [ ] GitLab's [Building an Inclusive Remote Culture page](https://about.gitlab.com/company/culture/inclusion/building-diversity-and-inclusion/)
   - [ ] Optionally, you can contribute by collaborating or creating a new [issue](https://gitlab.com/gitlab-com/diversity-and-inclusion/issues)

#### Additional Product Information
1. [ ] GitLab's Value Framework is foundational to GitLab's go-to-market messaging. We want to ensure all GitLab team members (even those outside of sales) are aware of what it is, why it is so important to our continued growth and success, and what it means to our customers, partners, and GitLab team members. Learn more in [this virtual session recording](http://bit.ly/37FqLC4) and the [Command of the Message](https://about.gitlab.com/handbook/sales/command-of-the-message/) handbook page.
1. [ ] Take a look at GitLab's [public customer list](https://about.gitlab.com/customers/) to learn about our list of advertised customers.
1. [ ] Take a look at GitLab's [product tier use-cases](https://about.gitlab.com/handbook/ceo/pricing/#three-tiers) and [product marketing tiers](https://about.gitlab.com/handbook/marketing/product-marketing/tiers/) to learn about GitLab product capabilities, tiers, and the motivation behind each tier.

#### Benefits Extras
1. [ ] Since we are a global organization understand your benefits might be different than other team members at the company based on which [contract](https://about.gitlab.com/handbook/contracts/) you signed. It is important to understand **your** [benefits](https://about.gitlab.com/handbook/benefits/). If you have questions please reach out to People Experience.
1. [ ] [No ask time off policy](https://about.gitlab.com/handbook/paid-time-off): GitLab truly does value a work-life balance, and encourages team members to have a flexible schedule and take vacations. If you feel uncomfortable about taking time off, or are not sure how much time to take off throughout the year, feel free to speak with your manager or People Experience. We will be happy to reinforce this policy! Please note the additional steps that might need to be taken if you are scheduled for [on call](https://about.gitlab.com/handbook/on-call).
1. [ ] Read about GitLab [stock options](https://about.gitlab.com/handbook/stock-options/). If you received stock options as part of your contract, your options will be approved by the Board of Directors at their [quarterly board meetings](https://about.gitlab.com/handbook/board-meetings/#board-meeting-schedule). After your grant has been approved by the Board you will receive a grant notice by email and access to Carta.

#### Surveys and Reviews
1. [ ] Set a reminder for 30 days past your start date to complete the [Onboarding Survey](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSdU8qxThxRu3tdrExFGBJ1GlyucCoJj2kDpcvOlM4AiRGOavQ/viewform) and kindly review [the OSAT](https://about.gitlab.com/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat). It is vitally important for us to continue getting feedback and iterate on onboarding.
1. [ ] If you feel comfortable, please create a video sharing your onboarding experience as per this [process](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-feedback/). This will definitely be helpful to all future new team members!
1. [ ] Help spread the word about life at GitLab. Set a reminder in sixty days to share your feedback by leaving a review on [Glassdoor](https://www.glassdoor.com/Overview/Working-at-GitLab-EI_IE1296544.11,17.htm) or [Comparably](https://www.comparably.com/companies/gitlab). We welcome positive and negative feedback.

</details>

### Job-specific tasks

---
<!-- include: role_tasks -->

<!-- include: people_manager_tasks -->

<!-- include: division_tasks -->

<!-- include: intern_tasks -->

---

<!-- include: department_tasks -->

---

