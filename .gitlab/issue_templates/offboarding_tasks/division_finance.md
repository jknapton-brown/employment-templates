<summary>Finance</summary>

1. [ ] Remove from Comerica, Rabobank or any other account (as user or viewer).
1. [ ] @mmaradiaga: Remove team member from Workiva
1. [ ] @edelongpre: Remove team member from FloQast
1. [ ] @edelongpre: Remove team member from Xactly CEA
1. [ ] Igor Groenewegen-Mackintosh @igroenewegenmackintosh: Remove team member from VATit SaaS
1. [ ] Igor Groenewegen-Mackintosh @igroenewegenmackintosh: Remove team member from Avalara
1. [ ] @mquitevis, @wlam and @awestbrook: Remove access from Zuora Revenue
1. [ ] @Swethakashyap, @lisapuzar, @racheldavies: Remove access from Xactly Incent
1. [ ] @rcshah @jburrows001 @mmaneval20: Remove the team member from ZenGRC

<summary>Manager</summary>

1. [ ] Manager: Remove from sales meeting.
