### FOR SECURITY ONLY

- [ ] @jritchey @estrike: Remove the team member from HackerOne
- [ ] @jritchey @estrike: Remove the team member from hackerone-customer.slack.com slack workspace
- [ ] @jurbanc: Remove the team member from Tenable.IO
- [ ] @jurbanc: Remove the team member from Rackspace (Security Enclave)
- [ ] @jurbanc: Remove the team member from AWS Security
- [ ] @jurbanc: Remove the team member from Panther SIEM
- [ ] @jurbanc: Remove the team member from MISP
- [ ] @rcshah @jburrows001 @mmaneval20: Remove the team member from ZenGRC
- [ ] @darawarde @sdaily: Remove the team member from Google Search Console
- [ ] @nsarosy: Remove team member from PhishLabs




